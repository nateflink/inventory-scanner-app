//
//  UIViewController+UIViewElements.h
//  WowOrder
//
//  Created by Nate Flink on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Elements)

+ (UIView *)positionViewToGrid:(UIView *)view atRow:(int)row andCol:(int)col;
+ (UILabel *)createRightAlignedLabel:(NSString *)text;
+ (UILabel *)createBigLabel:(NSString *)text;
+ (UIWebView *)createWebView:(NSString *)text;
+ (NSDictionary *)createLabelsInTableCell:(NSString *)descr and:(NSString *)info;
+ (UITextField *)createTextField:(NSString *)text;
+ (UIButton *)createButton:(NSString *)text;
+ (UITableView *)createTable;
+ (UILabel *)createLabel:(NSString *)text;
+ (UITableView *) createBigTable;
@end
