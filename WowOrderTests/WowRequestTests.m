//
//  WowServiceTests.m
//  WowOrder
//
//  Created by Nate Flink on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WowRequestTests.h"
#import "AppConfiguration.h"

@implementation WowRequestTests

- (void) setUp {
    [super setUp];
    NSLog(@"\n\n\n\n\n");
    [WowService sharedInstance].appId = @"1";
    [WowService sharedInstance].userId = @"1";
}
// All code under test must be linked into the Unit Test bundle

- (void) testCreateContext {
    NSString * str = [WowService createContext:@"foo", @"bar", nil];

    STAssertTrue([str isEqualToString:@"<foo>bar</foo>"], @"Error!");
}
- (void) testCreateContextBadArguments {
    NSString * str = [WowService createContext:@"foo", @"bar", @"asd", nil];

    STAssertFalse([str isEqualToString:@"<foo>bar</foo>"], @"Error!");
}
- (void) testXMLStringFromDictionary {
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:@"bar", @"foo", nil];

    STAssertTrue([[WowService XMLStringFromDictionary:dict] isEqualToString:@"<foo>bar</foo>"], @"fail");
}
- (void) testDataForCreateContextLongString {
    NSString * str = [WowService createContext:
                      @"ItemID", @"789",
                      @"ItemNumber", @"item789",
                      @"QuantityOrdered", @"99",
                      @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]; mk();

    NSString * str2 = @"<ItemID>789</ItemID><ItemNumber>item789</ItemNumber><QuantityOrdered>99</QuantityOrdered><UOM>EA</UOM>";
    STAssertTrue([str isEqualToString:str2], @"Fail!");
}
- (void) testDataForCreateSalesLine {
    NSData * data = [WowService dataForCreateSalesLine:10 with:
                     [WowService createContext:
                      @"ItemID", @"789",
                      @"ItemNumber", @"item789",
                      @"QuantityOrdered", @"99",
                      @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]
        ];

    NSData * data2 = JSON_FROM_KEYS_AND_OBJECTS(
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"CreateSalesLine",
            @"Context", @"<ItemID>789</ItemID><ItemNumber>item789</ItemNumber><QuantityOrdered>99</QuantityOrdered><UOM>EA</UOM>",
            @"ObjectNodeId", @"0",
            @"ParentObjectNodeId", [NSString stringWithFormat:@"%d", 10],
            @"ChildObjectNodeId", @"0",
            @"Put", @"-1",
            @"UserId", [WowService sharedInstance].userId);

    NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString * str2 = [[NSString alloc] initWithData:data2 encoding:NSUTF8StringEncoding];

    STAssertTrue([str isEqualToString:str2], @"fail");
}
- (void) testDataForChangeSalesLine {
    NSData * data = [WowService dataForChangeSalesLine:10 with:
                     [WowService createContext:
                      @"QuantityOrdered", @"99", nil]
        ];

    NSData * data2 = JSON_FROM_KEYS_AND_OBJECTS(
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"ChangeSalesLine",
            @"Context", @"<QuantityOrdered>99</QuantityOrdered>",
            @"ObjectNodeId", [NSString stringWithFormat:@"%d", 10],
            @"ParentObjectNodeId", @"-1",
            @"ChildObjectNodeId", @"-1",
            @"Put", @"-1",
            @"UserId", [WowService sharedInstance].userId);

    NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString * str2 = [[NSString alloc] initWithData:data2 encoding:NSUTF8StringEncoding];

    mk(); mk(); mk(); mk(); mk(); mk(); mk();
    DLog(@"generated: %@ ::::  test %@", str, str2);
    STAssertTrue([str isEqualToString:str2], @"fail");
}
@end
