//
//  WowRemoteObjectTests.m
//  WowOrder
//
//  Created by Nate Flink on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  This class unit tests the WoW API endpoint methods
//

#import "WowRemoteObjectTests.h"
#import "SenTestCase+Comparison.h"

//@implementation SenTestCase (Comparison)
//
//+ (BOOL) containsDigits:(NSString *)str;
//{
//    NSError * error = nil;
//    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:@"\\d+" options:NSRegularExpressionCaseInsensitive error:&error];
//    
//    NSUInteger numberOfMatches = [regex numberOfMatchesInString:str options:0 range:NSMakeRange(0, [str length])];
//    return numberOfMatches > 0;
//}
//
//@end

@implementation WowRemoteObjectTests

- (void) setUp {
    [super setUp];
    NSLog(@"\n\n\n\n\n");
    [WowService sharedInstance].appId = @"1";
    [WowService sharedInstance].userId = @"1";
}

- (void) testRemoteObjectPostJsonToCreateOrder {
    BOOL __block done = NO;
    NSData * data = [WowService dataForCreateOrder:
                     [WowService createContext:
                      @"RequestDeliveryDate", @"01/10/2012",
                      @"SiteID", @"122",
                      @"UserID", @"456", nil]
                     ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"create order get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}


- (void) testRemoteObjectPostJsonToCreateSalesLine {
    BOOL __block done = NO;
    NSData * data = [WowService dataForCreateSalesLine:162 with:
                     [WowService createContext:
                      @"ItemID", @"789",
                      @"ItemNumber", @"item789",
                      @"QuantityOrdered", @"99",
                      @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]
                     ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        STAssertTrue (![@"-1" isEqualToString:str], @"Fail! NewObjectID IS -1!"); // should be something like 330 or 110023
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}



- (void) testRemoteObjectPostJsonToShowSalesLine {
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowSalesLine:162 with:
                     [WowService createContext:
                      @"IncludeAttributes", @"1", nil]
                     ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        id jsonObject = JSON_TO_OBJ (data);
        NSString * str = [NSString stringWithFormat:@"%@", [jsonObject objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        STAssertTrue ([@"-1" isEqualToString:str], @"Fail! NewObjectID is NOT -1!");
        
        // assert that the output has a objects -> [0] -> attributes -> [0] -> AttributeID node key
        BOOL __block hasAnAttributeID = NO;
        NSDictionary * dict = [[[[jsonObject objectForKey:@"Objects"] objectAtIndex:0] objectForKey:@"Attributes"] objectAtIndex:0];
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
            if ([key isEqualToString:@"AttributeID"]) {
                hasAnAttributeID = YES;
            }
        }];
        STAssertTrue (hasAnAttributeID, @"fail, an attribute ID could not be found");
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}


// apparently, delete sales line won't return an error if you try to delete a sales line that has already been deleteed
- (void) testRemoteObjectPostJsonToDeleteSalesLine {
    BOOL __block done = NO;
    NSData * data = [WowService dataForDeleteSalesLine:163];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        // STAssertTrue ([@"-1" isEqualToString:str], @"Fail! NewObjectID is NOT -1!");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}
- (void) testRemoteObjectPostJsonToShowBarcode {
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowBarcode:
                     [WowService createContext:
                      @"IncludeAttributes", @"1",
                      @"Name", @"110111",
                      @"IncludeParents", @"1", nil]
                     ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains an Objects object and that that contains an ID node
        id jsonObject = JSON_TO_OBJ (data);
        NSString * str = [NSString stringWithFormat:@"%@", [jsonObject objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        // assert that the output has a objects -> [0] -> attributes -> [0] -> AttributeID node key
        BOOL __block hasAnAttributeID = NO;
        NSDictionary * dict = [[[[jsonObject objectForKey:@"Objects"] objectAtIndex:0] objectForKey:@"Attributes"] objectAtIndex:0];
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
            if ([key isEqualToString:@"AttributeID"]) {
                hasAnAttributeID = YES;
            }
        }];
        STAssertTrue (hasAnAttributeID, @"fail, an attribute ID could not be found");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}

- (void) testRemoteObjectPostJsonToShowOrdStatus {
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowOrdStatus:
                     [WowService createContext:
                      @"IncludeChildren", @"1", nil]
                     ];
    //sample output for showOrdStatus
    /*
     {
     "NewObjectID": -1,
     "Objects": [
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713186030-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.",
     "Id": 149,
     "Level": 0,
     "ModifiedDate": "\/Date(1330713186030-0600)\/",
     "ModifiedUserId": 0,
     "Name": "OrdStatus",
     "ParentId": 0,
     "TypeId": 155
     },
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713235293-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.151.",
     "Id": 151,
     "Level": 1,
     "ModifiedDate": "\/Date(1330713235293-0600)\/",
     "ModifiedUserId": 0,
     "Name": "New",
     "ParentId": 149,
     "TypeId": 157
     },
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713263350-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.151.152.",
     "Id": 152,
     "Level": 2,
     "ModifiedDate": "\/Date(1330713263350-0600)\/",
     "ModifiedUserId": 0,
     "Name": "Submitted",
     "ParentId": 151,
     "TypeId": 158
     },
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713272537-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.151.152.153.",
     "Id": 153,
     "Level": 3,
     "ModifiedDate": "\/Date(1330713272537-0600)\/",
     "ModifiedUserId": 0,
     "Name": "Review",
     "ParentId": 152,
     "TypeId": 159
     },
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713282640-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.151.152.153.154.",
     "Id": 154,
     "Level": 4,
     "ModifiedDate": "\/Date(1330713282640-0600)\/",
     "ModifiedUserId": 0,
     "Name": "Processing",
     "ParentId": 153,
     "TypeId": 160
     },
     {
     "Attributes": [
     {
     "AttributeID": -1,
     "AttributeName": null,
     "AttributeOrder": -1,
     "AttributeTypeId": -1,
     "AttributeValue": null
     }
     ],
     "CreatedDate": "\/Date(1330713294070-0600)\/",
     "CreatedUserId": -1,
     "Hierarchy": ".149.151.152.153.154.155.",
     "Id": 155,
     "Level": 5,
     "ModifiedDate": "\/Date(1330713294070-0600)\/",
     "ModifiedUserId": 0,
     "Name": "Shipped",
     "ParentId": 154,
     "TypeId": 161
     }
     ]
     }
     */
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk();mk();mk();mk();mk();mk();mk();
        DLog (@"showOrdStatus get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        NSMutableDictionary * statuses = [NSMutableDictionary new];
        [[JSON_TO_OBJ(data) objectForKey:@"Objects"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){            
            [statuses setValue:[obj objectForKey:@"Level"] forKey:[obj objectForKey:@"Name"]];
            if ([[obj objectForKey:@"Level"] intValue] == 0 && ![[obj objectForKey:@"Name"] isEqualToString:@"OrdStatus"])  {
                STFail(@"expected the value OrdStatus to be level 0");
            }
        }];
        STAssertTrue([statuses count] > 1, @"expected more than 1 status object");
        DLog(@"show statuses %@", statuses);
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}



- (void) testRemoteObjectPostJsonCrudSalesLine
//_ShowBarCode_CreateOrder_CreateSalesLine_ShowSalesLine_ChangeSalesLine_ShowSalesLine_DeleteSalesLine
{
    // show barcode - get the barcode item id
    // ============
    BOOL __block done;
    NSData * data;
    
    done = NO;
    data = [WowService dataForShowBarcode:
            [WowService createContext:
             @"IncludeAttributes", @"1",
             @"Name", @"110111",
             @"IncludeParents", @"1", nil]
            ];
    
    NSString * __block attributeItemNumber;
    NSNumber * __block objectId;
    
    // sample json
    /* sample output from showBarcode
     * {
     *  "NewObjectID": -1,
     *  "Objects": [
     *              {
     *                  "Attributes": [
     *                                 {
     *                                     "AttributeID": 24,
     *                                     "AttributeName": "Item Number",
     *                                     "AttributeOrder": 1,
     *                                     "AttributeTypeId": 129,
     *                                     "AttributeValue": "80100"
     *                                 },
     *                                 {
     *                                     "AttributeID": 25,
     *                                     "AttributeName": "Min Order Quantity",
     *                                     "AttributeOrder": 2,
     *                                     "AttributeTypeId": 139,
     *                                     "AttributeValue": "0"
     *                                 },
     *                                 {
     *                                     "AttributeID": 26,
     *                                     "AttributeName": "Min Order UOM",
     *                                     "AttributeOrder": 3,
     *                                     "AttributeTypeId": 140,
     *                                     "AttributeValue": null
     *                                 },
     *                                 {
     *                                     "AttributeID": 27,
     *                                     "AttributeName": "Max Order Quantity",
     *                                     "AttributeOrder": 4,
     *                                     "AttributeTypeId": 141,
     *                                     "AttributeValue": "0"
     *                                 },
     *                                 {
     *                                     "AttributeID": 28,
     *                                     "AttributeName": "Max Order UOM",
     *                                     "AttributeOrder": 5,
     *                                     "AttributeTypeId": 142,
     *                                     "AttributeValue": "PA"
     *                                 },
     *                                 {
     *                                     "AttributeID": 29,
     *                                     "AttributeName": "Base UOM Price",
     *                                     "AttributeOrder": 6,
     *                                     "AttributeTypeId": 143,
     *                                     "AttributeValue": "0.00"
     *                                 },
     *                                 {
     *                                     "AttributeID": 30,
     *                                     "AttributeName": "Adhere To Structure",
     *                                     "AttributeOrder": 7,
     *                                     "AttributeTypeId": 138,
     *                                     "AttributeValue": "1"
     *                                 }
     *                                 ],
     *                  "CreatedDate": "\/Date(1329840234690-0600)\/",
     *                  "CreatedUserId": 1,
     *                  "Hierarchy": ".141.",
     *                  "Id": 141,
     *                  "Level": 0,
     *                  "ModifiedDate": "\/Date(1329840234690-0600)\/",
     *                  "ModifiedUserId": 0,
     *                  "Name": "Red Box",
     *                  "ParentId": 0,
     *                  "TypeId": 137
     *              },
     *              {
     *                  "Attributes": [
     *                                 {
     *                                     "AttributeID": -1,
     *                                     "AttributeName": null,
     *                                     "AttributeOrder": -1,
     *                                     "AttributeTypeId": -1,
     *                                     "AttributeValue": null
     *                                 }
     *                                 ],
     *                  "CreatedDate": "\/Date(1329847378743-0600)\/",
     *                  "CreatedUserId": 1,
     *                  "Hierarchy": ".141.152.",
     *                  "Id": 152,
     *                  "Level": 1,
     *                  "ModifiedDate": "\/Date(1329847378743-0600)\/",
     *                  "ModifiedUserId": 0,
     *                  "Name": "110111",
     *                  "ParentId": 141,
     *                  "TypeId": 144
     *              }
     *              ]
     * }
     */
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk (); DLog (@"showBarcode get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains an Objects object and that that contains an ID node
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        
        // sample data
        /*
         * dictionary: {
         * "Adhere To Structure" = 1;
         * "Base UOM Price" = "0.00";
         * CreatedDate = "/Date(1329840234690-0600)/";
         * CreatedUserId = 1;
         * Hierarchy = ".141.";
         * Id = 141;
         * "Item Number" = 80100;
         * Level = 0;
         * "Max Order Quantity" = 0;
         * "Max Order UOM" = PA;
         * "Min Order Quantity" = 0;
         * "Min Order UOM" = "<null>";
         * ModifiedDate = "/Date(1329840234690-0600)/";
         * ModifiedUserId = 0;
         * Name = "Red Box";
         * ParentId = 0;
         * TypeId = 137;
         * }
         */
        
        DLog (@"barcode dictionary: %@", dict);
        STAssertTrue ([dict count] > 2, @"fail the dict is not the expected count");
        
        objectId = [dict objectForKey:@"Id"]; // data: [[[jsonObject objectForKey:@"Objects"] objectAtIndex:0] objectForKey:@"Id"];
        STAssertTrue ([objectId intValue] == 141, @"failure - item id is wrong");
        
        attributeItemNumber = [dict objectForKey:@"Item Number"];
        STAssertTrue ([@"80100" isEqualToString:attributeItemNumber], @"failure - Item Number attribute is wrong");
        
        DLog (@"attributeItemNumber value %@", objectId, [objectId intValue]);
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // create a new order
    // ==================
    done = NO;
    data = [WowService dataForCreateOrder:
            [WowService createContext:
             @"RequestDeliveryDate", @"01/10/2012",
             @"SiteID", @"122",
             @"UserID", @"456", nil]
            ];
    NSNumber * __block orderId;
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk ();
        DLog (@"createOrder get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        orderId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        
        NSString * str = [NSString stringWithFormat:@"%@", orderId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
    // create a new salesline
    // ======================
    done = NO;
    
    data = [WowService dataForCreateSalesLine:[orderId intValue] with:
            [WowService createContext:
             @"ItemID", objectId,
             @"ItemNumber", attributeItemNumber,
             @"QuantityOrdered", @"1",
             @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]
            ];
    NSNumber * __block salesLineId;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"createSalesLine get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        salesLineId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        NSString * str = [NSString stringWithFormat:@"%@", salesLineId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        STAssertTrue (![@"-1" isEqualToString:str], @"Fail! NewObjectID IS -1!"); // should be something like 330 or 110023
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // show salesline that was just created
    // ==============
    done = NO;
    data = [WowService dataForShowSalesLine:[salesLineId intValue] with:
            [WowService createContext:
             @"IncludeAttributes", @"1", nil]
            ];
    DLog (@"mark!");
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show salesline get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        
        mk (); mk (); mk ();
        DLog (@"show sales line dict: %@", dict);
        // example output
        /*
         * {
         * CreatedDate = "/Date(1331046151593-0600)/";
         * CreatedUserId = 1;
         * Hierarchy = ".443.444.";
         * Id = 444;
         * "Item Number" = 80100;
         * ItemID = 141;
         * Level = 1;
         * ModifiedDate = "/Date(1331046151593-0600)/";
         * ModifiedUserId = 0;
         * Name = "Sales Line";
         * ParentId = 443;
         * "Quantity Ordered" = 1;
         * "Quantity Shipped" = 0;
         * TypeId = 115;
         * UOM = EA;
         * }
         */
        
        STAssertTrue ([attributeItemNumber isEqualToString:[dict objectForKey:@"Item Number"]], @"fail item number is not correct");
        STAssertTrue ([objectId intValue] == [[dict objectForKey:@"ItemID"] intValue], @"fail item id is not correct");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // change the salesline
    // ====================
    done = NO;
    
    //remember quanity as teh quantity for later! ;)
    int __block quantityOrdered = arc4random() % 74;
    data = [WowService dataForChangeSalesLine:[salesLineId intValue] with:
            [WowService createContext:
             @"QuantityOrdered", [NSString stringWithFormat:@"%i",quantityOrdered], nil]
            ];
    
    
    
    [RemoteObject postJson : REMOTE_SERVICE_URL requestBody : data callback :^(NSData * data) {
        mk();mk();mk();mk();mk();
        DLog (@"change salesline get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // example output - Note: Thibuad said that there is no data reference check available, so detecting errors is impossible ;)
        /*
         * {"NewObjectID":10,"Objects":[]}
         */
        
        STAssertTrue ([salesLineId intValue] == [[JSON_TO_OBJ(data) objectForKey:@"NewObjectID"] intValue], @"fail");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
    // show salesline and check that the quantity was in fact changed successfully
    // ==============
    done = NO;
    data = [WowService dataForShowSalesLine:[salesLineId intValue] with:
            [WowService createContext:
             @"IncludeAttributes", @"1", nil]
            ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show sales line check quanity get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        
        mk (); mk (); mk ();
        DLog (@"show sales line output: %@", dict);
        // example output
        /*
         * {
         * CreatedDate = "/Date(1331046151593-0600)/";
         * CreatedUserId = 1;
         * Hierarchy = ".443.444.";
         * Id = 444;
         * "Item Number" = 80100;
         * ItemID = 141;
         * Level = 1;
         * ModifiedDate = "/Date(1331046151593-0600)/";
         * ModifiedUserId = 0;
         * Name = "Sales Line";
         * ParentId = 443;
         * "Quantity Ordered" = 1;
         * "Quantity Shipped" = 0;
         * TypeId = 115;
         * UOM = EA;
         * }
         */
        mk(); mk(); mk();
        DLog(@"Randomized Quantity Ordered: %i", quantityOrdered);
        //check that the quantity is what we set it
        STAssertTrue (quantityOrdered == [[dict objectForKey:@"Quantity Ordered"] intValue], @"fail quantity is not correct");
        
        //        STAssertTrue ([attributeItemNumber isEqualToString:[dict objectForKey:@"Item Number"]], @"fail item number is not correct");
        //        STAssertTrue ([objectId intValue] == [[dict objectForKey:@"ItemID"] intValue], @"fail item id is not correct");
        //
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    return;
    
    
    
#warning delete is broken server side, reenable the test when fixed
    //delete the sales line
    //=====================
    done = NO;
    
    data = [WowService dataForDeleteSalesLine:[salesLineId intValue]];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"delete sales line get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        //STAssertTrue ([@"-1" isEqualToString:str], @"Fail! NewObjectID is NOT -1!");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
    // show salesline once again to confirm that it has been deleted
    // ==============
    done = NO;
    data = [WowService dataForShowSalesLine:[salesLineId intValue] with:
            [WowService createContext:
             @"IncludeAttributes", @"1", nil]
            ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show sales line confirm delete get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ); 
        //Sales line delete doesn't work
        //Thibaud has said we aren't going to fix this right now, so this test is going to fail
        STAssertTrue([[JSON_TO_OBJ(data) objectForKey:@"Objects"] count] == 0, @"fail object still exists");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
}


- (void) testRemoteObjectPostJsonRetrieveSalesLines
{
    //return;
    // show barcode - get the   item id
    // ============
    BOOL __block done;
    NSData * data;
    
    done = NO;
    data = [WowService dataForShowBarcode:
            [WowService createContext:
             @"IncludeAttributes", @"1",
             @"Name", @"110111",
             @"IncludeParents", @"1", nil]
            ];
    
    NSString * __block attributeItemNumber;
    NSNumber * __block objectId;
    
    
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk (); DLog (@"showBarcode get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains an Objects object and that that contains an ID node
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        
        // sample data
        /*
         * dictionary: {
         * "Adhere To Structure" = 1;
         * "Base UOM Price" = "0.00";
         * CreatedDate = "/Date(1329840234690-0600)/";
         * CreatedUserId = 1;
         * Hierarchy = ".141.";
         * Id = 141;
         * "Item Number" = 80100;
         * Level = 0;
         * "Max Order Quantity" = 0;
         * "Max Order UOM" = PA;
         * "Min Order Quantity" = 0;
         * "Min Order UOM" = "<null>";
         * ModifiedDate = "/Date(1329840234690-0600)/";
         * ModifiedUserId = 0;
         * Name = "Red Box";
         * ParentId = 0;
         * TypeId = 137;
         * }
         */
        
        DLog (@"barcode dictionary: %@", dict);
        STAssertTrue ([dict count] > 2, @"fail the dict is not the expected count");
        
        objectId = [dict objectForKey:@"Id"]; // data: [[[jsonObject objectForKey:@"Objects"] objectAtIndex:0] objectForKey:@"Id"];
        STAssertTrue ([objectId intValue] == 141, @"failure - item id is wrong");
        
        attributeItemNumber = [dict objectForKey:@"Item Number"];
        STAssertTrue ([@"80100" isEqualToString:attributeItemNumber], @"failure - Item Number attribute is wrong");
        
        DLog (@"attributeItemNumber value %@", objectId, [objectId intValue]);
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // create a new order
    // ==================
    done = NO;
    data = [WowService dataForCreateOrder:
            [WowService createContext:
             @"RequestDeliveryDate", @"01/10/2012",
             @"SiteID", @"122",
             @"UserID", @"456", nil]
            ];
    NSNumber * __block orderId;
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk ();
        DLog (@"createOrder get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        orderId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        
        NSString * str = [NSString stringWithFormat:@"%@", orderId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
    // create 2 new saleslines
    // =======================
    done = NO;
    
    data = [WowService dataForCreateSalesLine:[orderId intValue] with:
            [WowService createContext:
             @"ItemID", objectId,
             @"ItemNumber", attributeItemNumber,
             @"QuantityOrdered", @"1",
             @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]
            ];
    NSNumber * __block salesLineId;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"createSalesLine get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        salesLineId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        NSString * str = [NSString stringWithFormat:@"%@", salesLineId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        STAssertTrue (![@"-1" isEqualToString:str], @"Fail! NewObjectID IS -1!"); // should be something like 330 or 110023
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    done = NO;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"creating 2nd SalesLine get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // assert that the output contains a value for the key NewObjectID and that it is at least one digit
        salesLineId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        NSString * str = [NSString stringWithFormat:@"%@", salesLineId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        STAssertTrue (![@"-1" isEqualToString:str], @"Fail! NewObjectID IS -1!"); // should be something like 330 or 110023
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // show saleslines that were just created
    // ===============
    done = NO;
    data = [WowService dataForShowOrder:[orderId intValue] with:
            [WowService createContext:
             @"IncludeChildren", @"1",
             @"IncludeAttributes", @"1",nil]
            ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show order includechildren get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        NSArray *arr = [WowService parseResponseForObjectsWithAttributes:data];
        DLog(@"the data show orders: %@",arr);
        
        STAssertTrue ([arr count] == 3, @"fail wrong number of objects were returned");
        STAssertTrue ([[[arr objectAtIndex:0] objectForKey:@"Name"] isEqualToString:@"Sales Line"] || [[[arr objectAtIndex:0] objectForKey:@"Name"] isEqualToString:@"Order"], @"fail name is not 'Sales Line' or 'Order'");
        
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
}
-(void) testShowItem; 
{
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowItem:[NSNumber numberWithInt:141] with:
                     [WowService createContext:
                      @"IncludeAttributes", @"1",
                      nil]
                     ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"showItem get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        DLog (@"showItem dict: %@",  dict);
        
        STAssertTrue([@"foo" isEqualToString:@"foo"], @"fail");
        
        STAssertTrue([[dict objectForKey:@"Item Number"] isEqualToString:@"80100"], @"fail the expected item does not have the correct Item NUmber");
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}
-(void) testChangeOrderStatus;
{
    BOOL __block done = NO;
    //sample output for showOrdStatus
    //===============================
    NSData * data = [WowService dataForShowOrdStatus:
                     [WowService createContext:
                      @"IncludeChildren", @"1", nil]
                     ];
    
    //    New = 1;
    //    OrdStatus = 0;
    //    Processing = 4;
    //    Review = 3;
    //    Shipped = 5;
    //    Submitted = 2;
    NSMutableDictionary * __block statuses = [NSMutableDictionary new];
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        
        DLog (@"showOrdStatus get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        [[JSON_TO_OBJ(data) objectForKey:@"Objects"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){            
            [statuses setValue:[obj objectForKey:@"Id"] forKey:[obj objectForKey:@"Name"]];
            if ([[obj objectForKey:@"Level"] intValue] == 0 && ![[obj objectForKey:@"Name"] isEqualToString:@"OrdStatus"])  {
                STFail(@"expected the value OrdStatus to be level 0");
            }
        }];
        STAssertTrue([statuses count] > 1, @"expected more than 1 status object");
        DLog(@"show statuses %@", statuses);
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    done = NO;
    //create order
    //============
    data = [WowService dataForCreateOrder:
            [WowService createContext:
             @"RequestDeliveryDate", @"01/10/2012",
             @"SiteID", @"122",
             @"UserID", @"456", nil]
            ];
    NSNumber * __block orderId;
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk ();
        DLog (@"createOrder get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        orderId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
        
        NSString * str = [NSString stringWithFormat:@"%@", orderId];
        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    //change order
    //============
    done = NO;
    
    data = [WowService dataForChangeOrder:[orderId intValue] with:
            [WowService createContext:@"StatusId",[statuses objectForKey:@"Submitted"],
             nil]
            ];
    DLog(@"request body for change order: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"change order status get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        //        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    // show sales order with new status
    // ===============
    done = NO;
    data = [WowService dataForShowOrder:[orderId intValue] with:
            [WowService createContext:
             @"IncludeChildren", @"1",
             @"IncludeAttributes", @"1",nil]
            ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show order includechildren get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        NSArray *arr = [WowService parseResponseForObjectsWithAttributes:data];
        DLog(@"the data show orders: %@",arr);
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    
    
}
-(void) testshowOrders;
{
    // show orders
    // ===============
    BOOL __block done = NO;
#warning should be dataForShowOrders:20     
    NSData * data = [WowService dataForShowOrder:1804 with:
            [WowService createContext:
             //@"IncludeChildren", @"1",
             @"IncludeAttributes", @"1",
             nil]
            ];
#warning debugging code
    NSMutableURLRequest * req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:REMOTE_SERVICE_URL]];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"text/json" forHTTPHeaderField:@"Accept"];
    [req setHTTPBody:data];
    [req setHTTPMethod:@"POST"];
    
    DLog(@"RemoteObject the NSMutableRequest [%@] allHTTPHeaderFields [%@] HTTPBody [%@] HTTPmethod [%@]", req, [req allHTTPHeaderFields], [[NSString alloc] initWithData:[req HTTPBody] encoding:NSUTF8StringEncoding], [req HTTPMethod]);

    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show orders get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
//        NSArray *arr = [WowService parseResponseForObjectsWithAttributes:data];
//        DLog(@"the data show orders: %@",arr);
        
        STAssertTrue ([[dict objectForKey:@"Id"] intValue] == 1804, @"fail wrong object was returned");
//        STAssertTrue ([[[arr objectAtIndex:0] objectForKey:@"Name"] isEqualToString:@"Sales Line"] || [[[arr objectAtIndex:0] objectForKey:@"Name"] isEqualToString:@"Order"], @"fail name is not 'Sales Line' or 'Order'");
        
//        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}
@end

