//
//  SenTestCase+Comparison.h
//  WowOrder
//
//  Created by Nate Flink on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface SenTestCase (Comparison)
+ (BOOL) containsDigits:(NSString *)str;
@end
