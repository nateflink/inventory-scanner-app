//
//  WowOrderTests.m
//  WowOrderTests
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WowOrderTests.h"
#import "SenTestCase+Comparison.h"


@implementation WowOrderTests

-(void) testInitWithCreateNewOrder;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    NSString * str = [NSString stringWithFormat:@"%@",[order.orderData objectForKey:@"ObjectNodeId"]];
    DLog(@" testInitWithCreateNewOrder order.orderData ID: [%@] dict: %@", str, order.orderData);
    STAssertTrue([SenTestCase containsDigits:str], @"fail");
}
//this test depends on a hard coded item id. data changes would expect to break it
-(void) testRetrieveBarcodeForItem;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    NSDictionary * item =  [order retrieveItemFromBarcode:@"110011"];
    NSNumber * itemId = [item objectForKey:@"Id"];
    STAssertTrue([itemId intValue] == 142, @"fail the id is wrong");
}
-(void) testAddSalesLineForItem;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    NSString * str = [NSString stringWithFormat:@"%@",[order.orderData objectForKey:@"ObjectNodeId"]];
    DLog(@" testAddSalesLineForItem order.orderData ID: [%@] dict: %@", str, order.orderData);
    //NSNumber * salesLineId = [order addSalesLineForItem:[NSNumber numberWithInt:142]];
    NSNumber * salesLineId = 
        [order addSalesLineForItem:[NSNumber numberWithInt:141] with:@"80100"];
    DLog(@" order.salesLineObjects %@", [order.salesLineObjects objectAtIndex:[order.salesLineIds indexOfObject:salesLineId]]);
    STAssertTrue([[[order.salesLineObjects objectAtIndex:[order.salesLineIds indexOfObject:salesLineId]] objectForKey:@"Id"] intValue] == [salesLineId intValue] , @"fail the id is not correct");
}
-(void) testSubmitOrder;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    STAssertTrue([[order retrieveStatusAsString] isEqualToString:@"New"],@"fail status is not correct");
    DLog(@"the order status before submitting is: %@",[order retrieveStatusAsString]);
    [order submitOrder];
    DLog(@"the order after submiting status is: %@",[order retrieveStatusAsString]);
    STAssertTrue([[order retrieveStatusAsString] isEqualToString:@"Submitted"],@"fail status is not correct");
}
-(void) testRetrieveOrderIds;
{
    NSArray * orderIds = [WowOrder retrieveOrderIds];
    for (int i=0; i<[orderIds count]; i++) {
        WowOrder * order = [[WowOrder alloc] initWithExistingOrderId:[orderIds objectAtIndex:i]];
       // [orders addObject:order];
        DLog(@"orderData:%@ - salesLineObjects%@",order.orderData, order.salesLineObjects);        
    }

}
-(void) testIncrementQuantityForItem;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    NSNumber * salesLineId = [order addSalesLineForItem:[NSNumber numberWithInt:141] with:@"80100"];
    NSNumber * quantity = [order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"];
    
    STAssertTrue([order salesLineExistsForItemId:[NSNumber numberWithInt:141]] == YES, @"fail the sales line doesnt exist");

    STAssertTrue([salesLineId intValue] == [[order salesLineIdForItemId:[NSNumber numberWithInt:141]] intValue], @"fail the sales line id is wrong");
    
    STAssertTrue([quantity intValue] == 1, @"fail the initial quantity is not correct");
    [order updateQuantityOrdered:[NSNumber numberWithInt:[quantity intValue]+1] forSalesLine:salesLineId];
    quantity = [order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"];
    
    STAssertTrue([quantity intValue] == 2, @"fail quantity is not correct");
}

-(void) testIncrementItem;
{
    WowOrder * order = [[WowOrder alloc] initWithCreateNewOrder];
    NSNumber * salesLineId = [order incrementItem:[NSNumber numberWithInt:141]  with:@"80100"];
    [order hydrate];
    NSNumber * quantity = [order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"];
    STAssertTrue([quantity intValue] == 1, @"the quantity is wrong %@",quantity);
    
    [order incrementItem:[NSNumber numberWithInt:141]  with:@"80100"];
    [order hydrate];
    quantity = [order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"];
    STAssertTrue([quantity intValue] == 2, @"the quantity is wrong %@",quantity);
    
    [order incrementItem:[NSNumber numberWithInt:141]  with:@"80100"];
    [order hydrate];
    quantity = [order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"];
    STAssertTrue([quantity intValue] == 3, @"the quantity is wrong %@",quantity);
}
@end
