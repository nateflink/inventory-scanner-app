//
//  RemoteObjectTests.m
//  WowOrder
//
//  Created by Nate Flink on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  These are generic tests for the RemoteObject
//  These tests are potentially quite fragile, as they depend on external URLS being up
//
//

#import "RemoteObjectTests.h"

@implementation RemoteObjectTests

- (void) setUp {
    [super setUp];
    NSLog(@"\n\n\n\n\n");
}

//- (void) testGetMethodUsingTwitter {
//    BOOL __block done = NO;
//
//    [RemoteObject get:@"http://search.twitter.com/search.json?q=Apple&rpp=1" callback:^(NSData * data) {
//         DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
//         NSString * response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//         STAssertTrue ([response rangeOfString:@"completed_in"].location != NSNotFound, @"fail");
//         done = YES;
//     }];
//    WaitFor (^BOOL { return done; }); // because this is async, Sen testing framework needs to wait for the response
//}

- (void) testGetMethodUsingGoogleSearchWithoutAPIKey {
    BOOL __block done = NO;

    // without an api key google actually returns a 403, but this means that our GET worked ;)
    [RemoteObject get:@"https://www.googleapis.com/customsearch/v1?q=flowers&alt=json" callback:^(NSData * data) {
         DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
         NSString * response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         STAssertTrue ([response rangeOfString:@"dailyLimitExceededUnreg"].location != NSNotFound, @"fail");
         done = YES;
     }];
    WaitFor (^BOOL { return done; });
}

- (void) testPostMethodUsingQuestionsCmsHhsGov {
    BOOL __block done = NO;
    // this one tests a post request against a .gov website.

    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://questions.cms.hhs.gov/ci/ajaxRequest/getReportData"]];

    [request setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];

    // basically sending an invalid post body
    NSData * str = [@"filters=" dataUsingEncoding:NSUTF8StringEncoding];
    [RemoteObject post:request requestBody:str callback:^(NSData * data) {
         DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
         NSString * response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         STAssertTrue ([response rangeOfString:@"report_id"].location != NSNotFound, @"fail");
         done = YES;
     }];
    WaitFor (^BOOL { return done; });
}

- (void) testPostJsonMethodUsingQuestionsCmsHhsGov {
    //   BOOL __block done = NO;

#warning this method needs to be implemented. Need to find a public web service to accept a POST json

    //   WaitFor(^BOOL { return done; });
}


@end
