//
//  SenTestCase+Comparison.m
//  WowOrder
//
//  Created by Nate Flink on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SenTestCase+Comparison.h"

@implementation SenTestCase (Comparison)

+ (BOOL) containsDigits:(NSString *)str;
{
    NSError * error = nil;
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:@"\\d+" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:str options:0 range:NSMakeRange(0, [str length])];
    return numberOfMatches > 0;
}

@end
