//
//  AppConfiguration.h
//  WowOrder
//
//  Created by Nate Flink on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// frameworks
#import "QuartzCore/QuartzCore.h"

// libraries
#import "LineaSDK.h"

// categories
#import "UIView+Elements.h"

// general purpose
#import "RemoteObject.h"
#import "TableListDataSource.h"

// specific to WowOrder
#import "WowService.h"
#import "WowOrder.h"

// controllers
#import "LoginViewController.h"
#import "SplashViewController.h"
#import "ItemListViewController.h"
#import "ItemDetailViewController.h"
#import "OrderStatusViewController.h"
#import "OrderHistoryViewController.h"
#import "ProfileViewController.h"
#import "SettingsViewController.h"

// variables
#define REMOTE_SERVICE_URL             @"http://demo.appolis.com/WcfWowOrder/Wow/"
//#define REMOTE_SERVICE_URL            @"http://wfw01.appint.local/WCFWowOrder/Wow/"
//#define REMOTE_SERVICE_URL            @"http://thibaudc-pc.appint.local/WCFWowOrder/Wow/"

#define REMOTE_SERVICE_CONNECT_SUCCESS @"client_connected"
#define REMOTE_SERVICE_DEFAULT_UOM     @"EA"
#define COMPANY_LOGO @"appolis-wow-order.png"


