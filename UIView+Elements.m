//
//  UIViewController+UIViewElements.m
//  WowOrder
//
//  Created by Nate Flink on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+Elements.h"
#import "UIView+Elements_Templates.h"


@implementation UIView (Elements)

+ (UIView *) positionViewToGrid:(UIView *)view atRow:(int)row andCol:(int)col;
{
    view.frame = CGRectMake(col * gridPixelUnit, row * gridPixelUnit, view.frame.size.width, view.frame.size.height);
    return view;
}
+ (UILabel *) createLabel:(NSString *)text;
{
    UILabel * obj = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
    WOLabel(obj);
    [obj setText:text];
    return obj;
}
+ (UILabel *) createRightAlignedLabel:(NSString *)text;
{
    UILabel * obj = [UIView createLabel:text];
    obj.textAlignment = UITextAlignmentRight;
    return obj;
}
+ (UILabel *) createBigLabel:(NSString *)text;
{
    UILabel * obj = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 20)];
    WOLabel(obj);
    [obj setText:text];
    return obj;
}
+ (UIWebView *) createWebView:(NSString *)text;
{
#warning refactor, abstract presentation
    UIWebView * obj = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    obj.opaque = NO;
    obj.backgroundColor = [UIColor clearColor];
    [obj loadHTMLString:[NSString stringWithFormat:@"<html>%@<body>%@</body></html>", WOWebViewStyle(), text] baseURL:[NSURL URLWithString:@""]];

    obj.userInteractionEnabled = NO;
    //    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat: @"%@/%@", [[NSBundle mainBundle] resourcePath], @"terms.html"]];
    //    [wv loadRequest:[NSURLRequest requestWithURL:url]];
    return obj;
}

//+ (NSDictionary *) createLabelsInTableCell:(NSString *)descr and:(NSString *)info;
//{
//#warning refactor - make size of cell dynamic?
//    UILabel * itemInfo = [[UILabel alloc] initWithFrame:CGRectMake(12, 8, 300, 10)];
//    [itemInfo setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
//    itemInfo.textColor = [UIColor blackColor];
//    itemInfo.text = info;
//
//    UILabel * itemDescr = [[UILabel alloc] initWithFrame:CGRectMake(12, 20, 300, 20)];
//    [itemDescr setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
//    itemDescr.textColor = [UIColor blackColor];
//    itemDescr.text = descr;
//
//    return [NSDictionary dictionaryWithObjectsAndKeys:itemInfo, @"itemInfo", itemDescr, @"itemDescr", nil];
//}

+ (UITextField *) createTextField:(NSString *)text;
{
    UITextField * obj = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
    WOTextField(obj);
    [obj setText:text];

    // [obj addTarget:self action:@selector(nil) forControlEvents:UIControlEventTouchUpInside];
    return obj;
}

+ (UIButton *) createButton:(NSString *)text;
{
    UIButton * obj = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    obj.frame = CGRectMake(0, 0, 160, 32);
    WOButton(obj);
    [obj setTitle:text forState:UIControlStateNormal];
    return obj;
}

+ (UITableView *) createTable;
{
    UITableView * obj = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 304, 264)];
    WOTable(obj);
    return obj;
}
+ (UITableView *) createBigTable;
{
    UITableView * obj = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 304, 320)];
    WOTable(obj);
    return obj;
}
@end
