//
//  SplashViewController.m
//  WowOrder
//
//  Created by Nate Flink on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


@implementation SplashViewController
@synthesize itemListViewController;

- (id) init {
    if (self = [super init]) {
     
    }
    return self;
}

 
- (void) viewDidLoad {
    [super viewDidLoad];

    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3_navbar.png"]];
    [self.view addSubview:bg];

    UIImageView * companyLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:COMPANY_LOGO]];
    companyLogo.frame = CGRectMake(10, 16, 300, 100);
    //[[companyLogo layer] setBorderColor:[[UIColor blackColor] CGColor]];
    //[[companyLogo layer] setBorderWidth:0.75];
    [self.view addSubview:companyLogo];

//TODO eventually get promotion from web service
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Current Promotions"] atRow:32 andCol:4]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createWebView:@"10% Off all discounted products until 03/22/2012, talk to Larry for details"] atRow:38 andCol:4]  ];

    
    UIButton * obj = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    obj.frame = CGRectMake(80, 290, 160, 32);
    [obj setTitleColor:[UIColor colorWithRed : 79 / 255.0 green : 107 / 255.0 blue : 137 / 255.0 alpha : 1.0] forState : UIControlStateNormal];
    obj.titleLabel.font = [UIFont fontWithName:@ "Helvetica-Bold" size:18];
    [obj setTitle:@"NEW ORDER" forState:UIControlStateNormal];
    [obj addTarget:self action:@selector(newOrderButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:obj];

    self.title = @"WoW Order";
}

- (void) newOrderButtonPressed:(id)sender {
    mk();
    [self.navigationController pushViewController:itemListViewController animated:YES];
}
- (void) viewWillAppear:(BOOL)animated {
    itemListViewController = nil;
    itemListViewController = [[ItemListViewController alloc] init];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [super viewWillAppear:animated];
}

@end
