//
//  WowService.m
//  WowOrder
//
//  Created by Nate Flink on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
// WowService singlton class is basically a factory for creating the
// requests to various REST endpoints avaialble from the WoW API ;)
//

#import "WowService.h"
static NSString * const NOVAL = @"-1";

@implementation WowService
@synthesize appId, userId, siteId;

- (id) init {
    self = [super init];
    if (self) {
        // Work your initialising magic here as you normally would
    }
    return self;
}

+ (WowService *) sharedInstance {
    static WowService * sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
                      sharedInstance = [[WowService alloc] init];
                      // Do any other initialisation stuff here
                  });
    return sharedInstance;
}
+ (NSString *) createContext:(NSString *)firstString, ...{
    NSMutableArray * arr = [NSMutableArray new];
    va_list args;

    va_start(args, firstString);
    for (NSString * arg = firstString; arg != nil; arg = va_arg(args, NSString *)) {

        [arr addObject:arg];
    }
    va_end(args);
    if (([arr count] % 2)) {
        return @"__CREATE_CONTEXT_MISSING_VALUE_ARGUMENT_FOR_KEY__";
    }
    NSMutableDictionary * dict = [NSMutableDictionary new];
    for (int i = 0; i < [arr count]; i += 2) {
        [dict setValue:[arr objectAtIndex:i + 1] forKey:[arr objectAtIndex:i]];
    }
    return [WowService XMLStringFromDictionary:dict];
}
+ (NSString *) XMLStringFromDictionary:(NSDictionary *)dict;
{
    NSString * __block str = @"";
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
         str = [NSString stringWithFormat:@"%@<%@>%@</%@>", str, key, obj, key];
     }];
    return str;
}
#pragma mark data for varius POST request factory methods
/*
 * ShowUser
 * ShowPromotion
 * CreateOrder
 * CreateSalesLine
 * ChangeSalesLine
 * DeleteSalesLine
 * ShowOrder
 * ShowSalesLine
 */
+ (NSData *) dataForCreateOrder:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"CreateOrder",
            @"Context", context,
            @"ObjectNodeId", @"0",
            @"ParentObjectNodeId", @"0",
            @"ChildObjectNodeId", @"0",
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}
+ (NSData *) dataForCreateSalesLine:(int)parentObjectNodeId with:(NSString *)context;
{

    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"CreateSalesLine",
            @"Context", context,
            @"ObjectNodeId", @"0",
            @"ParentObjectNodeId", [NSString stringWithFormat:@"%d", parentObjectNodeId],
            @"ChildObjectNodeId", @"0",
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}



/*
 * AppId: 1,
 * FunctionName: "ShowSalesLine",
 * Context: "<IncludeAttributes>1</ IncludeAttributes >",
 * ObjectNodeId: your ID,
 * ParentObjectNodeId: 0,
 * ChildObjectNodeId: 0,
 * Put: 1,
 * UserId: 1
 */

+ (NSData *) dataForShowSalesLine:(int)objectNodeId with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"ShowSalesLine",
            @"Context", context,
            @"ObjectNodeId", [NSString stringWithFormat:@"%d", objectNodeId],
            @"ParentObjectNodeId", NOVAL,
            @"ChildObjectNodeId", NOVAL,
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}
//    EXEC  [dbo].[OPS_CoreDirector]
//    @AppID = 1,
//    @FunctionName = N'ShowOrder',
//    @Context = N'<IncludeChildren>1</IncludeChildren>',
//    @ParentObjectNodeID = null,
//    @ObjectNodeID = 141,
//    @ChildObjectNodeID = NULL,
//    @PUT = NULL,
//    @UserID = NULL
//    return;
+ (NSData *) dataForShowOrder:(int)objectNodeId with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowOrder",
     @"Context", context,
     @"ObjectNodeId", [NSString stringWithFormat:@"%d", objectNodeId],
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", NOVAL,
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForShowOrders:(int)count with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowOrderShipped",
     @"Context", context,
     @"ObjectNodeId", NOVAL,
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", [NSString stringWithFormat:@"%d",count],
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForShowOrdersShipped:(int)count with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowOrderShipped",
     @"Context", context,
     @"ObjectNodeId", NOVAL,
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", [NSString stringWithFormat:@"%d",count],
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForShowOrdersNew:(int)count with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowOrderNew",
     @"Context", context,
     @"ObjectNodeId", NOVAL,
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", [NSString stringWithFormat:@"%d",count],
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForShowOrdersProcessing:(int)count with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowOrderProcessing",
     @"Context", context,
     @"ObjectNodeId", NOVAL,
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", [NSString stringWithFormat:@"%d",count],
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForChangeSalesLine:(int)objectNodeId with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"ChangeSalesLine",
            @"Context", context,
            @"ObjectNodeId", [NSString stringWithFormat:@"%d", objectNodeId],
            @"ParentObjectNodeId", NOVAL,
            @"ChildObjectNodeId", NOVAL,
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}

// AppId: 1,
// FunctionName: "DeleteSalesLine",
// Context: "",
// ObjectNodeId: your ID,
// ParentObjectNodeId: 0,
// ChildObjectNodeId: 0,
// Put: 1,
// UserId: 1

+ (NSData *) dataForDeleteSalesLine:(int)objectNodeId;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"DeleteSalesLine",
            @"Context", @"",
            @"ObjectNodeId", [NSString stringWithFormat:@"%d", objectNodeId],
            @"ParentObjectNodeId", NOVAL,
            @"ChildObjectNodeId", NOVAL,
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}

+ (NSData *) dataForShowBarcode:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"ShowBarcode",
            @"Context", context,
            @"ObjectNodeId", NOVAL,
            @"ParentObjectNodeId", NOVAL,
            @"ChildObjectNodeId", NOVAL,
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}
+ (NSData *) dataForShowItem:(NSNumber *)itemId with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ShowItem",
     @"Context", context,
     @"ObjectNodeId", itemId,
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", NOVAL,
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}
+ (NSData *) dataForShowOrdStatus:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
        (
            @"AppId", [WowService sharedInstance].appId,
            @"FunctionName", @"ShowOrdStatus",
            @"Context", context,
            @"ObjectNodeId", NOVAL,
            @"ParentObjectNodeId", NOVAL,
            @"ChildObjectNodeId", NOVAL,
            @"Put", NOVAL,
            @"UserId", [WowService sharedInstance].userId
        );
    return data;
}
//AppId: 1,
//                FunctionName: "ChangeOrder",
//                Context: "<StatusId>12</StatusId>",
//                ObjectNodeId: 150,
//                ParentObjectNodeId: 0,
//                ChildObjectNodeId: 0,
//                Put: 1,
//                UserId: 1

+ (NSData *) dataForChangeOrder:(int)objectNodeId with:(NSString *)context;
{
    NSData * data = JSON_FROM_KEYS_AND_OBJECTS
    (
     @"AppId", [WowService sharedInstance].appId,
     @"FunctionName", @"ChangeOrder",
     @"Context", context,
     @"ObjectNodeId", [NSString stringWithFormat:@"%d", objectNodeId],
     @"ParentObjectNodeId", NOVAL,
     @"ChildObjectNodeId", NOVAL,
     @"Put", NOVAL,
     @"UserId", [WowService sharedInstance].userId
     );
    return data;
}


#pragma mark
#pragma mark response

#warning TODO need to write a test for this
// This method could potentially overwrite duplicate keys
+ (NSDictionary *) parseResponseForObjectWithAttributes:(NSData *)jsonData;
{
    id jsonObject = [WowService parseResponseAsJsonObjects:jsonData];
    return [WowService enumerateObjectWithAttributes:jsonObject at:0];
}
//same as above, except creates an array of dictionaries example
/*
 {
 "Adhere To Structure" = 1;
 CreatedDate = "/Date(1331242926747-0600)/";
 ...
 UserID = 456;
 },
 {
 CreatedDate = "/Date(1331242926760-0600)/";
 ...
 "Quantity Shipped" = 0;
 TypeId = 115;
 UOM = EA;
 },
 {
 CreatedDate = "/Date(1331242926770-0600)/";
 ...
 TypeId = 115;
 UOM = EA;
 }
 */
+ (NSMutableArray *) parseResponseForObjectsWithAttributes:(NSData *)jsonData;
{
    id jsonObject = [WowService parseResponseAsJsonObjects:jsonData];
    NSMutableArray * arr = [NSMutableArray new];
    for (int i=0; i<[jsonObject count]; i++) {
        [arr addObject:[WowService enumerateObjectWithAttributes:jsonObject at:i]];
    }
    return arr;
}
+ (id) parseResponseAsJsonObjects:(NSData *)jsonData;
{
    NSError * parseError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&parseError];
    return [jsonObject objectForKey:@"Objects"];
}
+ (NSDictionary *) enumerateObjectWithAttributes:(id)jsonObject at:(int)index;
{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [[jsonObject objectAtIndex:index]
     enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
         
         if ([key isEqualToString:@"Attributes"]) {
             [obj enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx, BOOL * stop) {
                 [dict setValue:[obj2 objectForKey:@"AttributeValue"] forKey:[obj2 objectForKey:@"AttributeName"]];
             }];
             return;
         }
         [dict setValue:obj forKey:key];
     }];
    return dict;
}

@end
