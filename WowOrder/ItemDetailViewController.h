//
//  ItemDetailViewController.h
//  WowOrder
//
//  Created by Nate Flink on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailViewController : UIViewController  <UITextFieldDelegate>
@property (nonatomic, retain) WowOrder * order;
@property (nonatomic, retain) NSNumber * salesLineId;
@property (nonatomic, retain) UITextField * quantity;
- (id) initWithOrder:(WowOrder *)anOrder andSalesLineId:(NSNumber *)aSalesLineId;
@end
