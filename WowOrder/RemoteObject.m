//
//  RemoteObject.m
//  WowOrder
//
//  Created by Nate Flink on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RemoteObject.h"

@implementation RemoteObject
@synthesize receivedData, blockConnectionDidFinishLoading, blockConnectionDidFailWithError;

- (id) init {
    if (self = [super init]) {
        receivedData = [[NSMutableData alloc] init];
    }
    return self;
}

+ (RemoteObject *) get:(NSString *)url callback:(void (^)(NSData * data))block;
{
    NSMutableURLRequest * req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [req setHTTPMethod:@"GET"];

    RemoteObject * remoteObject = [[RemoteObject alloc] init];
    remoteObject.blockConnectionDidFinishLoading = [block copy]; // the copy prevents crashing here
    [NSURLConnection connectionWithRequest:req delegate:remoteObject];
    return remoteObject;
}
+ (RemoteObject *)get:(NSString *)url success:(void(^) (NSData * data))blockSuccess error:(void(^) (NSError * data))blockError;
{
    NSMutableURLRequest * req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [req setHTTPMethod:@"GET"];
    
    RemoteObject * remoteObject = [[RemoteObject alloc] init];
    remoteObject.blockConnectionDidFailWithError = [blockError copy];
    remoteObject.blockConnectionDidFinishLoading = [blockSuccess copy]; // the copy prevents crashing here
    [NSURLConnection connectionWithRequest:req delegate:remoteObject];
    return remoteObject;
}
+ (RemoteObject *) post:(NSMutableURLRequest *)request requestBody:(NSData *)data callback:(void (^)(NSData * data))block;
{
    NSMutableURLRequest * req = request;
    [req setHTTPBody:data];
    [req setHTTPMethod:@"POST"];
    NSString *msgLength = [NSString stringWithFormat:@"%d", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] length]];
    [req addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    
    DLog(@"RemoteObject the NSMutableRequest [%@] allHTTPHeaderFields [%@] HTTPBody [%@] HTTPmethod [%@]", req, [req allHTTPHeaderFields], [[NSString alloc] initWithData:[req HTTPBody] encoding:NSUTF8StringEncoding], [req HTTPMethod]);
    
    RemoteObject * remoteObject = [[RemoteObject alloc] init];
    remoteObject.blockConnectionDidFinishLoading = [block copy]; // the copy prevents crashing here
    [NSURLConnection connectionWithRequest:req delegate:remoteObject];

    return remoteObject;
}
+ (RemoteObject *) postJson:(NSString *)url requestBody:(NSData *)data callback:(void (^)(NSData * data))block;
{
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"text/json" forHTTPHeaderField:@"Accept"];
    return [RemoteObject post:request requestBody:data callback:block];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    DLog(@"didReceiveResponse");
    [receivedData setLength:0];
}
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    DLog(@"didReceiveData");
    [receivedData appendData:data];
}

/* call the error handler if our delegate uses one */
- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DLog(@"didFailWithError. Received [%@]", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    blockConnectionDidFailWithError(error);
//    NSMethodSignature * mySignature = [[delegator class] instanceMethodSignatureForSelector:delegatorErrorHandler];
//    NSInvocation * invc = [NSInvocation invocationWithMethodSignature:mySignature];
//    [invc setTarget:delegator];
//    [invc setSelector:delegatorErrorHandler];
//    [invc setArgument:&receivedData atIndex:2];
//    [invc invoke];
}
- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    DLog(@"remoteobject connectionDidFinishLoading Received [%@]", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    blockConnectionDidFinishLoading(receivedData);
      
//    NSMethodSignature * mySignature = [[delegator class] instanceMethodSignatureForSelector:delegatorSuccessHandler];
//    NSInvocation * invc = [NSInvocation invocationWithMethodSignature:mySignature];
//    [invc setTarget:delegator];
//    [invc setSelector:delegatorSuccessHandler];
//    [invc setArgument:&receivedData atIndex:2];
//    [invc invoke];
}

@end
