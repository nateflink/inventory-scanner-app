//
//  ItemDetailViewController.m
//  WowOrder
//
//  Created by Nate Flink on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//@implementation UITextField (custom2)
//- (CGRect) textRectForBounds:(CGRect)bounds { // 20 px height UITextField
//    return CGRectMake(bounds.origin.x + 8, bounds.origin.y + 8,
//                      bounds.size.width - 8, bounds.size.height - 2);
//}
//- (CGRect) editingRectForBounds:(CGRect)bounds {
//    return [self textRectForBounds:bounds];
//}
//@end

@implementation ItemDetailViewController
@synthesize order, salesLineId, quantity;

- (id) initWithOrder:(WowOrder *)anOrder andSalesLineId:(NSNumber *)aSalesLineId; 
{
    if (self = [super init]) {
       DLog(@"quantity: %@",[anOrder salesLine:aSalesLineId getAttributeByName:@"Quantity Ordered"]);
        order = anOrder;
        salesLineId = aSalesLineId;
        //selectedQuantity = [anOrder salesLine:aSalesLineId getAttributeByName:@"Quantity Ordered"];
    }
    return self;
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad {
    [super viewDidLoad];

    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3_navbar.png"]];
    [self.view addSubview:bg];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Account: 473729"] atRow:2 andCol:2]  ];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Item: Speciality Tool"] atRow:8 andCol:2]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Descr: Presorted grouping of spec. tools"] atRow:14 andCol:2]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Attribute: 100938"] atRow:20 andCol:2]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Attribute: 24.48 x 64 cm, W. 14"] atRow:26 andCol:2]  ];
    UILabel * obj = [UIView createLabel:@"Quantity:"];
    [obj setFont:[UIFont fontWithName:@ "HelveticaNeue" size:24]];
    [self.view addSubview:[UIView positionViewToGrid:obj atRow:36 andCol:2]];


    quantity = [UIView createTextField:[NSString stringWithFormat:@"%@",[order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"]]];
    [quantity setFont:[UIFont fontWithName:@"HelveticaNeue" size:28]];
    quantity.delegate = self;
    quantity.tag = 22;
    [quantity setKeyboardType:UIKeyboardTypeNumberPad];
    quantity.frame = CGRectMake(0, 0, 140, 36);
    quantity = (UITextField *)[UIView positionViewToGrid:quantity atRow:34 andCol:30];
    [[quantity layer] setCornerRadius:10];
    [quantity setClipsToBounds:YES];
    [[quantity layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[quantity layer] setBorderWidth:2.75];
    quantity.backgroundColor = [UIColor whiteColor];

    [self.view addSubview:quantity];
    [quantity becomeFirstResponder];

    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveOrderChangesPressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;

    // change the back button to cancel and add an event handler
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(cancelOrderChangesPressed:)];

    self.title = @"Item Quantity";

    self.navigationItem.leftBarButtonItem = cancelButton;
}
- (BOOL) textField: (UITextField *) textField shouldChangeCharactersInRange: (NSRange) range replacementString: (NSString *) string;
{
    DLog(@"quantity value did change %@", quantity.text   );
}

- (void) saveOrderChangesPressed:(id)sender {
    DLog(@"quantity value text value [%@] cast to NSNumber [%@]", quantity.text, [NSNumber numberWithInteger:[quantity.text integerValue]]);
    [order updateQuantityOrdered:[NSNumber numberWithInteger:[quantity.text integerValue]]
                    forSalesLine:salesLineId];
    
    [self.navigationController popViewControllerAnimated:YES];

}
- (void) cancelOrderChangesPressed:(id)sender {
    mk();
    //    ItemDetailViewController * i = [[ItemDetailViewController alloc] init];
    [self.navigationController popViewControllerAnimated:YES];

}
- (void) doNothingPressed:(id)sender {
    mk();
    //    ItemDetailViewController * i = [[ItemDetailViewController alloc] init];
    //    [self.navigationController pushViewController:i animated:YES];

}


@end
