//
//  ItemListViewController.m
//  WowOrder
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//



@implementation ItemListViewController
@synthesize linea, dataTable, order, tableListDataSource;

- (id) init {
    if (self = [super init]) {
       // receivedData = [[NSMutableData alloc] init];
        order = nil;
        tableListDataSource = nil;
    }
    return self;
}

-(id)initWithExistingOrder:(WowOrder *)anOrder;
{
    self = [self init];
    order = anOrder;//[[WowOrder alloc] initWithExistingOrderId:orderId hydrateOrder:NO];
    return self;
}
#pragma mark
#pragma mark view life cycle

-(void) viewWillAppear:(BOOL)animated {
    linea = [Linea sharedDevice];
    [linea connect];    
    [linea addDelegate:self];
    if (linea.connstate < 1) {
        DLog(@"Error. Linea connstate: %i ", linea.connstate);
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not connect to accessory" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        // [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    if (order != nil) {
        [order hydrate];
        [self updateTable];
        [self updatePropertyForViewWithTag:44 setValue:[NSString stringWithFormat:@"Company Inc.  -  Req Ship:%@",[order getAttributeByName:@"Request Delivery Date"]] forKey:@"text"];
        
        NSString * status = [order retrieveStatusAsString];
        [self updatePropertyForViewWithTag:56 setValue:[NSString stringWithFormat:@"%@  \t\t\t\t%@-%@",status, [order getAttributeByName:@"Name"], [order getAttributeByName:@"Id"]] forKey:@"text"];
        if ([status isEqualToString:@"New"]) {
            [self updatePropertyForViewWithTag:55 setValue:[UIColor greenColor] forKey:@"backgroundColor"];
        } else {
            [self updatePropertyForViewWithTag:55 setValue:[UIColor colorWithRed:0 / 255.0 green:0 / 255.0 blue:255 / 255.0 alpha:0.5] forKey:@"backgroundColor"];
        }

    }

}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3_navbar.png"]];
    [self.view addSubview:bg];
    
    UIView * companyLabel = [UIView positionViewToGrid:[UIView createBigLabel:@"Company Inc."] atRow:2 andCol:2];
    companyLabel.tag = 44;
    
    
    [self.view addSubview:companyLabel];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Orange WH"] atRow:8 andCol:2]  ];
    
    UIView * container = [[UIView alloc] initWithFrame:CGRectMake(8, 60, 260, 28)];
    container.tag = 55;
    
    [[container layer] setCornerRadius:10];
    [container setClipsToBounds:YES];
    
    //	// Create colored border using CALayer property
    [[container layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[container layer] setBorderWidth:2];
    container.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:container];
    
    
//    UIView * statusLabel = [UIView positionViewToGrid:[UIView createBigLabel:@"Scan to begin order"] atRow:16 andCol:4];
    
    UILabel * obj = [[UILabel alloc] initWithFrame:CGRectMake(20, 64, 300, 20)];
    obj.textColor = [UIColor blackColor];
    obj.backgroundColor = [UIColor clearColor];
    [obj setFont :[UIFont fontWithName : @ "HelveticaNeue" size : 16]];
    [obj setText:@"Scan to begin order"];
    obj.tag = 56;
    [self.view addSubview:obj];
    
    UIButton * inf = [UIButton buttonWithType:UIButtonTypeInfoDark];
    inf.center = CGPointMake(288, 72);
    [self.view addSubview:inf];
    [inf addTarget:self action:@selector(infoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    dataTable = [UIView createTable];
    dataTable.delegate = self;
    [self.view addSubview:[UIView positionViewToGrid:dataTable atRow:24 andCol:2]  ];
    
    if (order == nil) {//if this is a new order
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    }
    self.title = @"Items";
}
- (void) viewDidDisappear:(BOOL)animated {
    [linea disconnect];
    linea = nil;
}
#pragma mark
#pragma mark linea delegate methods
- (void) barcodeData:(NSString *)barcode type:(int)type;
{
    DLog(@"barcode data: %@", barcode);
    if (order == nil) {
        order = [[WowOrder alloc] initWithCreateNewOrder];
        [self updatePropertyForViewWithTag:44 setValue:@"Company Inc.  -  Req Ship:10/10/2012" forKey:@"text"];
        [self updatePropertyForViewWithTag:55 setValue:[UIColor greenColor] forKey:@"backgroundColor"];
        [self updatePropertyForViewWithTag:56 setValue:@"New" forKey:@"text"];
    }
    NSDictionary * item = [order retrieveItemFromBarcode:barcode];
    NSNumber *itemId = [item objectForKey:@"Id"];
    NSString * attributeItemNumber =  [item objectForKey:@"Item Number"];
    DLog(@"the item is: %@", itemId);
    if ([itemId intValue] == 0 || itemId == nil) {
        DLog(@"Error retrieving item from database");
        //TODO handle this error, item does not exist
        return;
    }
    //NSNumber * salesLineId = [order addSalesLineForItem:itemId with:attributeItemNumber];
    
    DLog(@"incrementing item on sales line");
    NSNumber * salesLineId = [order incrementItem:itemId with:attributeItemNumber];
    if ([[order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"] intValue] ==1) {
        ItemDetailViewController * i = [[ItemDetailViewController alloc] initWithOrder:order andSalesLineId:salesLineId];
        [self.navigationController pushViewController:i animated:NO]; 
    }
    [self updateTable];
}
- (void) buttonPressed:(int)which {
    DLog(@" Button pressed: %i", which);
}
- (void) buttonReleased:(int)which {
    
}
#pragma mark
#pragma mark app logic
-(void)updateTable;
{
    tableListDataSource = [[TableListDataSource alloc] initForItemListViewController];
    dataTable.dataSource = tableListDataSource;
    DLog(@"count sales line ids: %d - %@", [order.salesLineIds count], order.salesLineIds);
    for(int i=0; i<[order.salesLineIds count]; i++) {
        DLog(@"adding sales line id: %@ -- itemId: %@", [order.salesLineIds objectAtIndex:i], [order.itemIds objectAtIndex:i]);
        [self appendTableDataSourceDataForTable:tableListDataSource for:[order.itemIds objectAtIndex:i] with:[order.salesLineIds objectAtIndex:i]];
    }
    
    DLog(@"done with appendTable");
    [dataTable reloadData];
}

-(void)appendTableDataSourceDataForTable:(TableListDataSource *)aDataSource for:(NSNumber *)itemId with:(NSNumber *)salesLineId;
{
    
    DLog(@" added the sales line id to the data source:%@",salesLineId);
     
    [aDataSource.dataForTable addObject:DICTIONARY_WITH_KEYS_AND_OBJECTS(
            @"salesLineId",salesLineId,
            @"Item Number",[NSString stringWithFormat:@"%@", 
                            [order item:itemId getAttributeByName:@"Item Number"]],
            @"Name",[NSString stringWithFormat:@"%@",
                     [order item:itemId getAttributeByName:@"Name"]],
            @"UOM",[NSString stringWithFormat:@"%@",[order salesLine:salesLineId getAttributeByName:@"UOM"]],
            @"Quantity Ordered",[NSString stringWithFormat:@"%@",[order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"]],
            )]; 
    DLog(@"done adding sales line");
}

+(UITableViewCell *) addSubViewsToCell:(UITableViewCell *)cell forData:(NSDictionary *)tableCellData;
{
    UILabel * obj;
    obj = [[UILabel alloc] initWithFrame:CGRectMake(12, 6, 180, 20)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Name"];
    //obj.backgroundColor = [UIColor lightGrayColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(12, 28, 300, 10)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Item Number"];
    // obj.backgroundColor = [UIColor lightGrayColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(208, 6, 60, 20)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:18]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Quantity Ordered"];
    obj.textAlignment = UITextAlignmentRight;
    // obj.backgroundColor = [UIColor lightGrayColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(208, 28, 60, 10)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"UOM"];
    obj.textAlignment = UITextAlignmentRight;
    //obj.backgroundColor = [UIColor yellowColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    
    
    //create an "greater than" > in teh cell, requested by Travis
    obj = [[UILabel alloc] initWithFrame:CGRectMake(280, 10, 24, 24)];
    obj.textColor = [UIColor lightGrayColor];
    obj.backgroundColor = [UIColor clearColor];
    [obj setFont :[UIFont fontWithName : @ "HelveticaNeue" size : 24]];
    [obj setText:@">"];
    obj.userInteractionEnabled = NO;
    [cell addSubview:obj];
    return cell;
}

- (void) updatePropertyForViewWithTag:(int)tag setValue:(id)value forKey:(id)property;
{
    DLog(@"passed tag %d", tag);
    [self.view.subviews enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:
     ^(UILabel * subview, NSUInteger idx, BOOL * stop) {
         DLog (@"\tValue: %i Index: %d", subview.tag, idx);
         if (subview.tag == tag) {
             DLog(@"subview text %@",subview);
             // subview.text = text;
             [subview setValue:value forKey:property]; 
             DLog(@"subview text %@",subview);
             *stop = YES;
         }
     }
     ];
}
#pragma mark 
#pragma mark touch events
- (void) infoButtonPressed:(id)sender {
    OrderStatusViewController * i = [[OrderStatusViewController alloc] init];

    [self.navigationController pushViewController:i animated:YES];
}

- (void) doneButtonPressed:(id)sender {
    if (order != nil) {
        [[[UIAlertView alloc] initWithTitle:@"Submitting Order" message:@"Your order is being submitted to the server" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        [order submitOrder];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Table view delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * tableCellData = [tableListDataSource.dataForTable objectAtIndex:indexPath.row];
    
#warning TODO TODO TODO lastSalesLineCreated needs to get fixed!
    ItemDetailViewController * i = [[ItemDetailViewController alloc] initWithOrder:order andSalesLineId:[tableCellData objectForKey:@"salesLineId"]];
    //DLog(@"item list quantity: %@",[order salesLine:salesLineId getAttributeByName:@"Quantity Ordered"]);
    [self.navigationController pushViewController:i animated:NO];
}


@end
