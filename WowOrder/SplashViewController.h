//
//  SplashViewController.h
//  WowOrder
//
//  Created by Nate Flink on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemListViewController.h"
@interface SplashViewController : UIViewController
@property (nonatomic, retain)  ItemListViewController * itemListViewController;
@end
