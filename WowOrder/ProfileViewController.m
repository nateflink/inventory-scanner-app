//
//  ProfileViewController.m
//  WowOrder
//
//  Created by Nate Flink on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileViewController.h"
#import "AppConfiguration.h"

@implementation ProfileViewController

- (void) viewDidLoad {
    [self.view setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3.png"]];
    [self.view addSubview:bg];

    [super viewDidLoad];


    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"ID:"] atRow:12 andCol:6]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"4321"] atRow:12 andCol:38]  ];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"Username:"] atRow:22 andCol:6]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"John_D"] atRow:22 andCol:38]  ];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"First Name:"] atRow:32 andCol:6]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"John"] atRow:32 andCol:38]  ];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"Last Name:"] atRow:42 andCol:6]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Doe"] atRow:42 andCol:38]  ];

}

@end
