//
//  WowService.h
//  WowOrder
//
//  Created by Nate Flink on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WowService : NSObject
+ (WowService *)sharedInstance;
+ (NSString *)createContext:(NSString *)firstString, ... NS_REQUIRES_NIL_TERMINATION;
+ (NSString *)XMLStringFromDictionary:(NSDictionary *)dict;

+ (NSData *)dataForCreateSalesLine:(int)parentObjectNodeId with:(NSString *)context;
+ (NSData *)dataForCreateOrder:(NSString *)context;
+ (NSData *)dataForShowItem:(NSString *)context;
+ (NSData *)dataForShowSalesLine:(int)objectNodeId with:(NSString *)context;
+ (NSData *)dataForShowOrder:(int)objectNodeId with:(NSString *)context;
+ (NSData *) dataForShowOrders:(int)count with:(NSString *)context;
+ (NSData *)dataForChangeSalesLine:(int)objectNodeId with:(NSString *)context;
+ (NSData *)dataForDeleteSalesLine:(int)objectNodeId;
+ (NSData *) dataForShowOrdStatus:(NSString *)context;
+ (NSData *)dataForShowBarcode:(NSString *)context;
+ (NSData *) dataForShowItem:(NSNumber *)itemId with:(NSString *)context;
+ (NSData *) dataForChangeOrder:(int)objectNodeId with:(NSString *)context;

+ (NSData *) dataForShowOrdersShipped:(int)count with:(NSString *)context;
+ (NSData *) dataForShowOrdersNew:(int)count with:(NSString *)context;
+ (NSData *) dataForShowOrdersProcessing:(int)count with:(NSString *)context;


+ (NSDictionary *)parseResponseForObjectWithAttributes:(NSData *)jsonData;
+ (NSMutableArray *) parseResponseForObjectsWithAttributes:(NSData *)jsonData;
+ (id) parseResponseAsJsonObjects:(NSData *)jsonData; 
+ (NSDictionary *) enumerateObjectWithAttributes:(id)jsonObject at:(int)index;
@property (nonatomic, retain) NSString * appId;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * siteId;
@end
