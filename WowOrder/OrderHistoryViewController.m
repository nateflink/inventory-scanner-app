//
//  OrderHistoryViewController.m
//  WowOrder
//
//  Created by Nate Flink on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrderHistoryViewController.h"
#import "AppConfiguration.h"

@implementation OrderHistoryViewController
@synthesize dataSource, dataTable, orders;

- (id) init {
    if (self = [super init]) {
        // receivedData = [[NSMutableData alloc] init];
        orders = [NSMutableArray new];
    }
    return self;
}
-(void) updateTable:(NSString *)params; 
{
    dataSource = [[TableListDataSource alloc] initForOrderHistoryViewController];
    NSArray * orderIds;
    if ([params isEqualToString:@""]) {
        orderIds = [WowOrder retrieveOrderIds];
    } else {
        orderIds = [WowOrder retrieveOrderIdsFrom:params];
    }
    WowOrder * order;
    for (int i=0; i<[orderIds count]; i++) {
        order = [[WowOrder alloc] initWithExistingOrderId:[orderIds objectAtIndex:i]];
        DLog(@"adding order to table: %@", [order.orderData objectForKey:@"Id"]);
        [orders addObject:order];
        [self appendTableDataSourceDataForTable:dataSource for:order];
    }
    dataTable.dataSource = dataSource;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3_navbar.png"]];
    [self.view addSubview:bg];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"ORDER HISTORY"] atRow:30 andCol:22]  ];
    
    
    UIButton * obj;
    obj = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    obj.frame = CGRectMake(10, 12, 90, 32);
    [obj setTitleColor:[UIColor colorWithRed : 79 / 255.0 green : 107 / 255.0 blue : 137 / 255.0 alpha : 1.0] forState : UIControlStateNormal];
    obj.titleLabel.font = [UIFont fontWithName:@ "Helvetica-Bold" size:12];
    [obj setTitle:@"Shipped" forState:UIControlStateNormal];
    [obj addTarget:self action:@selector(shippedButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:obj];

    obj = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    obj.frame = CGRectMake(110, 12, 90, 32);
    [obj setTitleColor:[UIColor colorWithRed : 79 / 255.0 green : 107 / 255.0 blue : 137 / 255.0 alpha : 1.0] forState : UIControlStateNormal];
    obj.titleLabel.font = [UIFont fontWithName:@ "Helvetica-Bold" size:12];
    [obj setTitle:@"Processing" forState:UIControlStateNormal];
    [obj addTarget:self action:@selector(processingButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:obj]; 

    obj = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    obj.frame = CGRectMake(210, 12, 90, 32);
    [obj setTitleColor:[UIColor colorWithRed : 79 / 255.0 green : 107 / 255.0 blue : 137 / 255.0 alpha : 1.0] forState : UIControlStateNormal];
    obj.titleLabel.font = [UIFont fontWithName:@ "Helvetica-Bold" size:12];
    [obj setTitle:@"New" forState:UIControlStateNormal];
    [obj addTarget:self action:@selector(newButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:obj];
    
    
    
    dataTable = [[UITableView alloc] initWithFrame:CGRectMake(8, 54, 304, 300)];
    [[dataTable layer] setCornerRadius : 10];
    [[dataTable layer] setBorderColor : [[UIColor grayColor] CGColor]];
    [[dataTable layer] setBorderWidth : 2.75];
    [dataTable setClipsToBounds : YES];
    dataTable.delegate = self;
    [self updateTable:@"Shipped"];
    [self.view addSubview:dataTable];
    
}
#pragma mark - controller logic
-(void)appendTableDataSourceDataForTable:(TableListDataSource *)aDataSource for:(WowOrder *)order;
{

    [aDataSource.dataForTable addObject:DICTIONARY_WITH_KEYS_AND_OBJECTS
        (
        
        @"Name",[NSString stringWithFormat:@"Ord# %@", 
                        [order.orderData objectForKey:@"Name"]],
        @"Status",[NSString stringWithFormat:@"%@",
                 [order retrieveStatusAsString]],
        @"Request Delivery Date",[NSString stringWithFormat:@"%@",[order.orderData objectForKey:@"Request Delivery Date"]]
        )];
}

+(UITableViewCell *) addSubViewsToCell:(UITableViewCell *)cell forData:(NSDictionary *)tableCellData;
{

    
    UILabel * obj;
    obj = [[UILabel alloc] initWithFrame:CGRectMake(12, 6, 180, 20)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Name"];
    //obj.backgroundColor = [UIColor lightGrayColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(120, 6, 172, 20)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:18]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Request Delivery Date"];
    obj.textAlignment = UITextAlignmentRight;
    // obj.backgroundColor = [UIColor lightGrayColor];
    obj.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:obj];
    
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(120, 28, 172, 10)];
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    obj.textColor = [UIColor blackColor];
    obj.text = [tableCellData objectForKey:@"Status"];
    //obj.backgroundColor = [UIColor yellowColor];
    obj.adjustsFontSizeToFitWidth = YES;
    obj.textAlignment = UITextAlignmentRight;
    [cell addSubview:obj];
    
    return cell;
}
#pragma mark - Touch Events
- (void) shippedButtonPressed:(id)sender {
    [self updateTable:@"Shipped"];
    [dataTable reloadData];
}
- (void) processingButtonPressed:(id)sender {
    [self updateTable:@"Processing"];
    [dataTable reloadData];
}
- (void) newButtonPressed:(id)sender {
    [self updateTable:@"New"];
    [dataTable reloadData];
}
#pragma mark - Table view delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WowOrder * order = [orders objectAtIndex:indexPath.row];
    DLog(@"order loading id: %@",[order getAttributeByName:@"ObjectNodeId"] );
    ItemListViewController * i = [[ItemListViewController alloc] initWithExistingOrder:order];
    [self.navigationController pushViewController:i animated:NO];
}
@end
