//
//  AppDelegate.m
//  WowOrder
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "AppConfiguration.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize loginViewController = _loginViewController;
@synthesize navigationController = _navigationController;
@synthesize itemlistViewController = _itemlistViewController;
@synthesize tabBarController = _tabBarController;
@synthesize isLoggedIn;


- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    isLoggedIn = NO;
#warning TODO some thought should be put into where these should go
    [WowService sharedInstance].appId = @"1";
    [WowService sharedInstance].userId = @"1";
    [WowService sharedInstance].siteId = @"1";
    // set up a local nav controller which we will reuse for each view controller
    UINavigationController * localNavigationController;

    // create tab bar controller and array to hold the view controllers
    _tabBarController = [[UITabBarController alloc] init];
    NSMutableArray * localControllersArray = [[NSMutableArray alloc] initWithCapacity:4];

    // setup the first view controller (Root view controller)
    LoginViewController * myViewController;
    myViewController = [[LoginViewController alloc] init];

    // create the nav controller and add the root view controller as its first view
    localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController];
    myViewController.title = @"Home";
    myViewController.tabBarItem.image = [UIImage imageNamed:@"53-house.png"];
    // add the new nav controller (with the root view controller inside it)
    // to the array of controllers
    [localControllersArray addObject:localNavigationController];

    OrderHistoryViewController * orderHistoryViewController;
    orderHistoryViewController = [[OrderHistoryViewController alloc] init];
    orderHistoryViewController.title = @"History";
    orderHistoryViewController.tabBarItem.image = [UIImage imageNamed:@"11-clock.png"];
    orderHistoryViewController.navigationItem.title = @"History";
    localNavigationController = [[UINavigationController alloc] initWithRootViewController:orderHistoryViewController];
    [localControllersArray addObject:localNavigationController];

    // setup the second view controller just like the first
    ProfileViewController * profileViewController;
    profileViewController = [[ProfileViewController alloc] init];
    profileViewController.title = @"Profile";
    profileViewController.tabBarItem.image = [UIImage imageNamed:@"111-user.png"];
    profileViewController.navigationItem.title = @"Profile";
    localNavigationController = [[UINavigationController alloc] initWithRootViewController:profileViewController];
    [localControllersArray addObject:localNavigationController];

    // setup the third view controller just like the first
    SettingsViewController * settingsViewController;
    settingsViewController = [[SettingsViewController alloc] init];
    settingsViewController.title = @"Settings";
    settingsViewController.tabBarItem.image = [UIImage imageNamed:@"20-gear2.png"];
    settingsViewController.navigationItem.title = @"Settings";
    localNavigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    [localControllersArray addObject:localNavigationController];


    // load up our tab bar controller with the view controllers
    _tabBarController.viewControllers = localControllersArray;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    // add the tabBarController as a subview in the window
    [self.window addSubview:_tabBarController.view];

    // need this last line to display the window (and tab bar controller)
    [self.window makeKeyAndVisible];
    return YES;

}

#pragma mark boilerplate app delegate below ..

- (void) applicationWillResignActive:(UIApplication *)application {
    /*
     * Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     * Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void) applicationDidEnterBackground:(UIApplication *)application {
    /*
     * Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     * If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void) applicationWillEnterForeground:(UIApplication *)application {
    /*
     * Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void) applicationDidBecomeActive:(UIApplication *)application {
    /*
     * Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void) applicationWillTerminate:(UIApplication *)application {
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void) saveContext {
    NSError * error = nil;
    NSManagedObjectContext * managedObjectContext = self.managedObjectContext;

    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            /*
             * Replace this implementation with code to handle the error appropriately.
             *
             * abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

/**
 * Returns the managed object context for the application.
 * If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }

    NSPersistentStoreCoordinator * coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 * Returns the managed object model for the application.
 * If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *) managedObjectModel {
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL * modelURL = [[NSBundle mainBundle] URLForResource:@"WowOrder" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 * Returns the persistent store coordinator for the application.
 * If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *) persistentStoreCoordinator {
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }

    NSURL * storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"WowOrder.sqlite"];

    NSError * error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         * Replace this implementation with code to handle the error appropriately.
         *
         * abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         *
         * Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         * Check the error message to determine what the actual problem was.
         *
         *
         * If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         *
         * If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         * [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         *
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         * [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         *
         * Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         *
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 * Returns the URL to the application's Documents directory.
 */
- (NSURL *) applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
