//
//  UIViewController+UIViewElements_Templates.h
//  WowOrder
//
//  Created by Nate Flink on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define gridPixelUnit       4

#define paragraph_textColor [UIColor blackColor]
#define navButton_textColor [UIColor colorWithRed : 79 / 255.0 green : 107 / 255.0 blue : 137 / 255.0 alpha : 1.0]

#import "QuartzCore/QuartzCore.h"

static UILabel * WOLabel(UILabel * obj) {
    obj.textColor = paragraph_textColor;
    obj.backgroundColor = [UIColor clearColor];
    [obj setFont :[UIFont fontWithName : @ "HelveticaNeue" size : 16]];
    return obj;
}

static UITextField * WOTextField(UITextField * obj) {
    obj.textColor = paragraph_textColor;
    obj.textAlignment = UITextAlignmentLeft;
    obj.backgroundColor = [UIColor whiteColor];
    [obj setFont :[UIFont fontWithName : @ "HelveticaNeue" size : 14]];
    return obj;
}

static UIButton * WOButton(UIButton * obj) {
    [obj setTitleColor : navButton_textColor forState : UIControlStateNormal];
    obj.titleLabel.font = [UIFont fontWithName:@ "Helvetica-Bold" size:18];
    return obj;
}

static UITableView * WOTable(UITableView * obj) {
    [[obj layer] setCornerRadius : 10];
    [[obj layer] setBorderColor : [[UIColor grayColor] CGColor]];
    [[obj layer] setBorderWidth : 2.75];
    [obj setClipsToBounds : YES];
    return obj;
}
static NSString * WOWebViewStyle() {
    return @ "<style>* {margin:0;padding:0;background-color:transparent;font:14px HelveticaNeue;color:black}</style>";
}
