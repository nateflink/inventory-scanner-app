//
//  WowOrder.m
//  WowOrder
//
//  Created by Nate Flink on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  This class provides a simplified object-relation mapping for use by the view controllers
//

#import "WowOrder.h"

@implementation WowOrder
@synthesize orderData, salesLineIds, salesLineObjects, itemIds, itemObjects, orderStatuses;

-(id)init;
{
    self = [super init];
    if (self) {

        orderStatuses = [self retrieveOrderStatusObjects]; //TODO this shouldn't need to get called a lot       
    }
    return self;
}
-(id)initWithCreateNewOrder;
{
    BOOL __block done = NO;
    NSData * data = [WowService dataForCreateOrder:
            [WowService createContext:
             @"RequestDeliveryDate", @"12/12/2012",
             @"SiteID", [WowService sharedInstance].siteId,
             @"UserID", [WowService sharedInstance].userId, nil]
            ];
    
    NSNumber * __block orderId;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        mk (); mk (); mk ();
        DLog (@"createOrder get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );            
        orderId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
#warning TODO handle error with order creation

        //[orderData setValue:orderId forKey:@"ObjectNodeId"]; //this key should change
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    self = [self initWithExistingOrderId:orderId];
    return self;
}
-(id)initWithExistingOrderId:(NSNumber *)orderId hydrateOrder:(BOOL)hydrate;
{
    self = [self init];
    [self.orderData setObject:orderId forKey:@"ObjectNodeId"];
    if (hydrate == YES) {
        [self hydrate:orderId];
    }
    return self;
}
-(id)initWithExistingOrderId:(NSNumber *)orderId;
{
    self = [self init];
    [self hydrate:orderId];
    return self;
}
-(void)hydrate;
{
    [self hydrate:[self.orderData objectForKey:@"ObjectNodeId"]];
}
-(void)hydrate:(NSNumber *)orderId;
{
    orderData = nil;
    orderData = [NSMutableDictionary new];
    salesLineIds = nil;
    salesLineIds = [NSMutableArray new];
    salesLineObjects = nil;
    salesLineObjects = [NSMutableArray new];
    itemObjects = nil;
    itemObjects = [NSMutableArray new];
    itemIds = nil;
    itemIds = [NSMutableArray new];
    
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowOrder:[orderId intValue] with:
            [WowService createContext:
             @"IncludeChildren", @"1",
             @"IncludeAttributes", @"1",nil]
            ];
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show order includechildren get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        NSArray *arr = [WowService parseResponseForObjectsWithAttributes:data];
        DLog(@"the data show orders: %@",arr);
        orderData = (NSMutableDictionary *)[arr objectAtIndex:0]; //assumes the order is always return at index position 0
        #warning TODO change ObjectNodeId to be the same key as returned by ShowOrder ('Id')? and slurp in rest of order data after create order has been called
        [orderData setValue:[[arr objectAtIndex:0] objectForKey:@"Id"] forKey:@"ObjectNodeId"];//TODO should just be able to use "Id" key from now on
        //example output
        /*{
            "Adhere To Structure" = 1;
            CreatedDate = "/Date(1331245640830-0600)/";
            CreatedUserId = 1;
            Hierarchy = ".1209.";
            Id = 1209;
            Level = 0;
            ModifiedDate = "/Date(1331245640830-0600)/";
            ModifiedUserId = 0;
            Name = Order;
            ParentId = 0;
            "Request Delivery Date" = "10/10/2012";
            SiteID = 1;
            StatusID = 151;
            TypeId = 113;
            UserID = 1;
        },
        {
            CreatedDate = "/Date(1331245640957-0600)/";
            CreatedUserId = 1;
            Hierarchy = ".1209.1210.";
            Id = 1210;
            "Item Number" = 80100;
            ItemID = 141;
            Level = 1;
            ModifiedDate = "/Date(1331245640957-0600)/";
            ModifiedUserId = 0;
            Name = "Sales Line";
            ParentId = 1209;
            "Quantity Ordered" = 12;
            "Quantity Shipped" = 0;
            TypeId = 115;
            UOM = EA;
        }*/

        //skip the first item, first is garenteed to be the object?
        //get the attributes and hydrate the WowOrder obj
       
        for(int i=1; i<[arr count]; i++) {
            [salesLineIds addObject:[[arr objectAtIndex:i] objectForKey:@"Id"]];
            [salesLineObjects addObject:[arr objectAtIndex:i]];
            [itemIds addObject:[[arr objectAtIndex:i] objectForKey:@"ItemID"]];
            
            DLog(@"count itemIds %i -obj: %@", [itemIds count], itemIds);
            
            //do an additional web service call to look up item from id
            //TODO checking for handling an error if the item doesn't exist, or has a bad id?
            [self retrieveItemObjectForItemId:[[arr objectAtIndex:i] objectForKey:@"ItemID"]];                       
        }        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
}
-(NSDictionary *) retrieveOrderStatusObjects;
{
    BOOL __block done = NO;
    //sample output for showOrdStatus
    //===============================
    NSData * data = [WowService dataForShowOrdStatus:
                     [WowService createContext:
                      @"IncludeChildren", @"1", nil]
                     ];
    
    //    New = 1;
    //    OrdStatus = 0;
    //    Processing = 4;
    //    Review = 3;
    //    Shipped = 5;
    //    Submitted = 2;
    NSMutableDictionary * __block statuses = [NSMutableDictionary new];
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        
        DLog (@"showOrdStatus get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        [[JSON_TO_OBJ(data) objectForKey:@"Objects"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){  
            if ([[obj objectForKey:@"Name"] isEqualToString:@"OrdStatus"])  {
                return; //skip the "OrdStatus
            }
            [statuses setValue:[obj objectForKey:@"Id"] forKey:[obj objectForKey:@"Name"]];            
        }];
        DLog(@"show statuses %@", statuses);
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    return statuses;
}
-(BOOL)retrieveItemObjectForItemId:(NSNumber *)itemId;
{
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowItem:itemId with:
                     [WowService createContext:
                      @"IncludeAttributes", @"1",
                      nil]
                     ];
    BOOL __block itemExists = NO;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"showItem get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        DLog (@"showItem dict: %@",  dict);
        if ([dict count]>0) {
            itemExists = YES;
            [itemObjects insertObject:dict atIndex:[itemIds indexOfObject:itemId]];
        }
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    return itemExists;
}
-(NSString*)retrieveStatusAsString;
{
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowOrder:[[orderData objectForKey:@"ObjectNodeId"] intValue] with:
            [WowService createContext:
             //@"IncludeChildren", @"1",
             @"IncludeAttributes", @"1",nil]
            ];
    NSDictionary * __block dict;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show order includechildren get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //NSDictionary * dict = [WowService parseResponseForObjectWithAttributes:data];
        dict = [WowService parseResponseForObjectWithAttributes:data];
        DLog(@"retrieveStatusAsString data for show orders: %@",dict);
        
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    NSString * __block status;
    [orderStatuses enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {     
        DLog(@"status enumerate key: [%@] - obj: [%@] - obj int [%i] - dict intValue [%i]", key, obj, [obj intValue], [[dict objectForKey:@"StatusID"] intValue]);
        if ([obj intValue] == [[dict objectForKey:@"StatusID"] intValue]) {
            status = key;
            *stop = YES;
        }
        
    }];
    
    DLog(@"the status of the order is: %@",status);
    return status;
}
-(NSDictionary*)retrieveItemFromBarcode:(NSString *)barcode;
{
    BOOL __block done = NO;
    NSData * data = [WowService dataForShowBarcode:
                     [WowService createContext:
                      @"IncludeAttributes", @"1",
                      @"Name", barcode,
                      @"IncludeParents", @"1", nil]
                     ];
    NSDictionary * __block dict;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"showBarcode get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        dict = [WowService parseResponseForObjectWithAttributes:data];
//        itemId = [dict objectForKey:@"Id"];
//        [itemIds addObject:itemId];
//        [itemObjects insertObject:dict atIndex:[itemIds indexOfObject:itemId]];
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    return dict;
}
-(id)getAttributeByName:(NSString *)key;
{
    return [orderData objectForKey:key];
}
-(id)item:(NSNumber *)itemId getAttributeByName:(NSString *)key;
{
    return [[itemObjects objectAtIndex:[itemIds indexOfObject:itemId]] objectForKey:key];
}
-(id)salesLine:(NSNumber *)salesLineId getAttributeByName:(NSString *)key;
{
    return [[salesLineObjects objectAtIndex:[salesLineIds indexOfObject:salesLineId]] objectForKey:key];
}

-(NSNumber*)addSalesLineForItem:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
{
    BOOL __block done = NO;
//    NSString * attributeItemNumber = [[itemObjects objectAtIndex:[itemIds indexOfObject:itemId]] objectForKey:@"Item Number"];
    
    DLog(@" attributeItemNumber: %@", attributeItemNumber);
    
    NSData * data = [WowService dataForCreateSalesLine:[[orderData objectForKey:@"ObjectNodeId"] intValue] with:
            [WowService createContext:
             @"ItemID", itemId,
             @"ItemNumber", attributeItemNumber,
             @"QuantityOrdered", @"1",
             @"UOM", REMOTE_SERVICE_DEFAULT_UOM, nil]
            ];
    NSNumber * __block salesLineId;
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"addSalesLineForItem get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        salesLineId = [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"];
       // [salesLineIds addObject:salesLineId]; //no need this value should get hydrated
        done = YES;
        
    }];
    WaitFor (^BOOL { return done; });
    [self hydrate];
   
    return salesLineId; //a null or 0 salesLineId would be an error condition the caller should handle
}

-(void)updateQuantityOrdered:(NSNumber *)quantity forSalesLine:(NSNumber *)salesLineId;
{

    BOOL __block done = NO;
    NSData * data = [WowService dataForChangeSalesLine:[salesLineId intValue] with:
            [WowService createContext:
             @"QuantityOrdered", [NSString stringWithFormat:@"%@",quantity], nil]
            ];
    
    [RemoteObject postJson : REMOTE_SERVICE_URL requestBody: data callback:^(NSData * data) {
        mk();mk();mk();
        DLog (@"change salesline get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        // example output - Note: Thibuad said that there is no data reference check available, so detecting errors is impossible ;)
        /*
         * {"NewObjectID":10,"Objects":[]}
         */
   
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    [self hydrate];
}
-(BOOL)submitOrder;
{
    BOOL __block done = NO;
     
    NSData * data = [WowService dataForChangeOrder:[[orderData objectForKey:@"ObjectNodeId"] intValue] with:
            [WowService createContext:@"StatusId",[orderStatuses objectForKey:@"Submitted"],
             nil]
            ];
    DLog(@"request body for change order: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"change order status get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //        NSString * str = [NSString stringWithFormat:@"%@", [JSON_TO_OBJ (data) objectForKey:@"NewObjectID"]];
        //        STAssertTrue ([SenTestCase containsDigits:str], @"failure");
        
        done = YES;
    }];
    [self retrieveOrderStatusObjects];
    return YES;
}
#define ORDERS_LIMIT 8
+(NSArray *)retrieveOrderIds; 
{
    NSData * data = [WowService dataForShowOrders:ORDERS_LIMIT with:
                     [WowService createContext:
                      //@"IncludeChildren", @"1",
                     // @"IncludeAttributes", @"1",
                      nil]
                     ];
    return [WowOrder retrieveOrderIds:data]; 
}
+(NSArray *)retrieveOrderIdsFrom:(NSString *)params;
{
    NSData * data;
    if ([params isEqualToString:@"New"]) {
        data = [WowService dataForShowOrdersNew:ORDERS_LIMIT with:[WowService createContext:nil]];
        return [WowOrder retrieveOrderIds:data];
    } else if ([params isEqualToString:@"Processing"]) {
        data = [WowService dataForShowOrdersProcessing:ORDERS_LIMIT with:[WowService createContext:nil]];
        return [WowOrder retrieveOrderIds:data];
    } else if ([params isEqualToString:@"Shipped"]) {
        data = [WowService dataForShowOrdersShipped:ORDERS_LIMIT with:[WowService createContext:nil]];
        return [WowOrder retrieveOrderIds:data];
    }
        DLog(@"Error, a param was sent that we cant understand");
    return nil;
}
+(NSArray *)retrieveOrderIds:(NSData *)data; 
{
    BOOL __block done = NO;
    NSMutableArray * __block arr = [NSMutableArray new];
    [RemoteObject postJson:REMOTE_SERVICE_URL requestBody:data callback:^(NSData * data) {
        DLog (@"show orders get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
        
        //NSDictionary * dict = 
        arr =[WowService parseResponseForObjectsWithAttributes:data];
        //[arr addObject:[dict objectForKey:@"Id"]];
        done = YES;
    }];
    WaitFor (^BOOL { return done; });
    NSMutableArray * ids = [NSMutableArray new];
    [arr enumerateObjectsUsingBlock:^(id dict, NSUInteger idx, BOOL * stop) {
        [ids addObject:[dict objectForKey:@"Id"]];
    }];
    return ids;
}
-(BOOL)salesLineExistsForItemId:(NSNumber *)itemId;
{
    DLog(@"value of i: %i - count itemIds %i -obj: %@ -itemId:%@ -int %i", [itemIds indexOfObject:itemId], [itemIds count], itemIds, itemId, [itemId intValue]);
    
    for (int i=0; i<[itemIds count]; i++) {
        if ([[itemIds objectAtIndex:i] intValue] == [itemId intValue]) {
            return YES;
        }
    }
    return NO;
}
-(NSNumber *)salesLineIdForItemId:(NSNumber *)itemId;
{
    DLog(@"salesLineIdForItemId itemId [%@] - itemIds [%@] - salesLineIds [%@]", itemId, itemIds, salesLineIds);
    NSNumber * __block salesLineId;
    [salesLineObjects enumerateObjectsUsingBlock:^(id dict, NSUInteger idx, BOOL * stop) {
        if ([[dict objectForKey:@"ItemID"] intValue] == [itemId intValue]) {
            salesLineId = [dict objectForKey:@"Id"];
            *stop = true;
        }
    }];
    return salesLineId;
}
/* Note: that incrementItem does NOT call hydrate (repopulate remote data) on itself like add sales line does at present */
 -(NSNumber *)incrementItem:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
{
        DLog(@"incrementingItem");
    return [self incrementItemBy:1 forItemId:itemId  with:attributeItemNumber];
}
-(NSNumber *)incrementItemBy:(int)increment forItemId:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
{
    NSNumber * salesLineId;
    
    DLog(@"incrementingItemBy");
    if([self salesLineExistsForItemId:itemId] == YES) {
        DLog(@"incrementingItemBy salesLineIdForItemId");
        salesLineId = [self salesLineIdForItemId:itemId];
        DLog(@"the sale line id is %@", salesLineId);
        
        NSMutableDictionary * salesLineObject = [salesLineObjects objectAtIndex:[salesLineIds indexOfObject:salesLineId]];   
        DLog(@"sales line object [%@]", salesLineObject);
        
        int newQuantity = increment+[[salesLineObject valueForKey:@"Quantity Ordered"] intValue];
        [self updateQuantityOrdered:[NSNumber numberWithInt:newQuantity] forSalesLine:salesLineId];
    } else {
        salesLineId = [self addSalesLineForItem:itemId with:attributeItemNumber];       
    }
    
     
    return salesLineId;
}
@end
