//
//  LoginViewController.m
//  WowOrder
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "AppDelegate.h"


@implementation UITextField (custom)
- (CGRect) textRectForBounds:(CGRect)bounds { // 20 px height UITextField
    return CGRectMake(bounds.origin.x + 4, bounds.origin.y + 1,
                      bounds.size.width - 8, bounds.size.height - 2);
}
- (CGRect) editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end


@implementation UIViewController (tabbar)
- (void) hideTabBar:(UITabBarController *)tabbarcontroller {


    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    for (UIView * view in tabbarcontroller.view.subviews) {
        if ([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
        } else {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
    }
    [UIView commitAnimations];
}

- (void) showTabBar:(UITabBarController *)tabbarcontroller {

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    for (UIView * view in tabbarcontroller.view.subviews) {
        NSLog(@"%@", view);

        if ([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];

        } else {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
        }
    }
    [UIView commitAnimations];
}
@end
@implementation LoginViewController

- (void) viewDidLoad {
    [super viewDidLoad];

    // if database is unreachable, show an alert
    [RemoteObject get:REMOTE_SERVICE_URL success:^(NSData * data) {
         DLog (@"get output: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] );
         NSString * response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         if ([response rangeOfString:REMOTE_SERVICE_CONNECT_SUCCESS].location == NSNotFound) {
             [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not connect to server. Please restart app and try again" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             return;
         }
         DLog (@"Success connecting to service. Response: %@ ", response);
    } error:^(NSError * data) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Could not connect. [%@]",data] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        return;
    }];

//    SplashViewController * i = [[SplashViewController alloc] init];
//    [self.navigationController pushViewController:i animated:NO];
//    return;

    
    [self.view setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3.png"]];
    [self.view addSubview:bg];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"LOGIN"] atRow:30 andCol:2]  ];

    UITextField * username = [UIView createTextField:@""];
    username.delegate = self;
    username.tag = 50;
    [self.view addSubview:[UIView positionViewToGrid:username atRow:30 andCol:40]  ];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createRightAlignedLabel:@"PASSWORD"] atRow:38 andCol:2]  ];

    UITextField * password = [UIView createTextField:@""];
    password.secureTextEntry = YES; // this is the same as other text field, except it is a password
    [self.view addSubview:[UIView positionViewToGrid:password atRow:38 andCol:40]  ];
    password.delegate = self;
    password.tag = 60;

//    UIButton * continueButton = [UIView createButton:@"Continue"];
//    [continueButton addTarget:self action:@selector(continueButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:[UIView positionViewToGrid:continueButton atRow:80 andCol:20]  ];
//
}
// - (void) continueButtonPressed:(id)sender {
//    mk();
//    SplashViewController * i = [[SplashViewController alloc] init];
//    [self.navigationController pushViewController:i animated:NO];
// }

- (BOOL) textFieldShouldReturn:(UITextField *)textField {

    __block UIView * textFieldWithFocus = textField;

    [self.view.subviews enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:
     ^(UIView * subView, NSUInteger idx, BOOL * stop) {
        // DLog (@"\tValue: %i Index: %d", subView.tag, idx);
         if (subView.tag == 60 && textFieldWithFocus.tag == 50) {
             [(UITextField *) subView becomeFirstResponder];
             *stop = YES;
         } else if (subView.tag == 60 && textFieldWithFocus.tag == 60) {

             // TODO: Authentication code here
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
             appDelegate.isLoggedIn = YES;
             
             SplashViewController * i = [[SplashViewController alloc] init];
             [self.navigationController pushViewController:i animated:NO];
             [textFieldWithFocus resignFirstResponder];
         }
     }
    ];
    return YES;
}
- (void) viewWillAppear:(BOOL)animated {
    
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // DLog(@"is logged in: %d", appDelegate.isLoggedIn);
    if (appDelegate.isLoggedIn == YES) {
        SplashViewController * i = [[SplashViewController alloc] init];
        [self.navigationController pushViewController:i animated:NO];
        return;
    }
    
    [self hideTabBar:self.tabBarController];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    
    
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self showTabBar:self.tabBarController];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

@end
