//
//  ItemListDataSource.h
//  WowOrder
//
//  Created by Nate Flink on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TableListDataSource : NSObject <UITableViewDataSource>
@property (nonatomic, retain) NSMutableArray * dataForTable;
@property (nonatomic, retain) NSString * controller;
-(id) initForItemListViewController;
-(id) initForOrderHistoryViewController;
@end
