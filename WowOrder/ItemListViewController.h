//
//  ItemListViewController.h
//  WowOrder
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ItemListViewController : UIViewController <UITableViewDelegate, UITabBarDelegate, LineaDelegate>
//DOes this guy need to be a UITabBArDelegate???
@property (nonatomic, retain) Linea * linea;
@property (nonatomic, retain) UITableView * dataTable;
@property (nonatomic, retain) WowOrder * order;
@property (nonatomic, retain) TableListDataSource * tableListDataSource; 


-(id)initWithExistingOrder:(WowOrder *)anOrder;

-(void) updatePropertyForViewWithTag:(int)tag setValue:(id)value forKey:(id)property;
-(void)appendTableDataSourceDataForTable:(TableListDataSource *)aDataSource for:(NSNumber *)itemId with:(NSNumber *)salesLineId;
-(void)updateTable;
+(UITableViewCell *) addSubViewsToCell:(UITableViewCell *)cell forData:(NSDictionary *)tableCellData;
@end
