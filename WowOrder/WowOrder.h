//
//  WowOrder.h
//  WowOrder
//
//  Created by Nate Flink on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WowOrder : NSObject

@property (nonatomic, retain) NSMutableDictionary * orderData;
@property (nonatomic, retain) NSMutableArray * salesLineIds;
@property (nonatomic, retain) NSMutableArray * salesLineObjects;
@property (nonatomic, retain) NSMutableArray * itemIds;
@property (nonatomic, retain) NSMutableArray * itemObjects;
@property (nonatomic, retain) NSDictionary * orderStatuses;

-(id)initWithCreateNewOrder;
-(id)initWithExistingOrderId:(NSNumber *)orderId;
-(id)initWithExistingOrderId:(NSNumber *)orderId hydrateOrder:(BOOL)hydrate;

-(void)hydrate;
-(void)hydrate:(NSNumber *)orderId;

-(NSDictionary *) retrieveOrderStatusObjects;
-(id)getAttributeByName:(NSString *)key;
-(id)item:(NSNumber *)itemId getAttributeByName:(NSString *)key;
-(id)salesLine:(NSNumber *)salesLineId getAttributeByName:(NSString *)key;

-(BOOL)retrieveItemObjectForItemId:(NSNumber *)itemId;
-(NSString*)retrieveStatusAsString;
-(NSDictionary*)retrieveItemFromBarcode:(NSString *)barcode;
-(NSNumber*)addSalesLineForItem:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
-(void)updateQuantityOrdered:(NSNumber *)quantity forSalesLine:(NSNumber *)salesLineId;
-(BOOL)submitOrder;

+(NSArray *)retrieveOrderIds;
+(NSArray *)retrieveOrderIds:(NSData *)data; 
+(NSArray *)retrieveOrderIdsFrom:(NSString *)params;

-(BOOL)salesLineExistsForItemId:(NSNumber *)itemId;
-(NSNumber *)salesLineIdForItemId:(NSNumber *)itemId;

-(NSNumber *)incrementItem:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
-(NSNumber *)incrementItemBy:(int)increment forItemId:(NSNumber *)itemId with:(NSString *)attributeItemNumber;
@end
