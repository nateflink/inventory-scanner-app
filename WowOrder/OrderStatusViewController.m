//
//  OrderStatusViewController.m
//  WowOrder
//
//  Created by Nate Flink on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrderStatusViewController.h"
#import "AppConfiguration.h"

@implementation OrderStatusViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad {
    [self.view setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3_navbar.png"]];
    [self.view addSubview:bg];

    [super viewDidLoad];

    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"Status of Order 123123-234"] atRow:2 andCol:2]  ];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:@"John Doe"] atRow:8 andCol:2]  ];

    /*
     New = 1;
     OrdStatus = 0;
     Processing = 4;
     Review = 3;
     Shipped = 5;
     Submitted = 2;
     */
    
    //these next 3 lines are overly complicated, maybe use a dict [status -> i]
    NSArray * statusIds = [NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil];
    NSNumber * currentStatusId = [NSNumber numberWithInt:1];
    NSUInteger currentStatusIndex = [statusIds indexOfObject:[NSNumber numberWithInt:1]];

    NSArray * statuses = [NSArray arrayWithObjects:@"New", @"Submitted", @"Review", @"Processing", @"Shipped", nil];
    UIView * __block container;

    [statuses enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
         container = [[UIView alloc] initWithFrame:CGRectMake (12, 86 + (idx * 36), 298, 26)];
         container.backgroundColor = [UIColor clearColor];

         [[container layer] setCornerRadius:10];
         [container setClipsToBounds:YES];

         // Create colored border using CALayer property
         [[container layer] setBorderColor:[[UIColor blackColor] CGColor]];
         [[container layer] setBorderWidth:1];

         container.backgroundColor = [UIColor colorWithRed:0 / 255.0 green:0 / 255.0 blue:(100 + (idx * (255 / [statuses count]))) / 255.0 alpha:0.5];
         if (idx == currentStatusIndex) {
             container.backgroundColor = [UIColor clearColor];
         }

         UILabel * statusLabel = [[UILabel alloc] initWithFrame:CGRectMake (8, 0, 290, 28)];
         statusLabel.text = [statuses objectAtIndex:idx];
         [statusLabel setFont:[UIFont fontWithName:@ "HelveticaNeue" size:16]];
         statusLabel.backgroundColor = [UIColor clearColor];
         [container addSubview:statusLabel];
         [self.view addSubview:container];

     }];

}


@end
