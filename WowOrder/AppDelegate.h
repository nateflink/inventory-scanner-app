//
//  AppDelegate.h
//  WowOrder
//
//  Created by Nate Flink on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppConfiguration.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@property (readonly, strong, nonatomic) NSManagedObjectContext * managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (nonatomic, retain) UINavigationController * navigationController;
@property (nonatomic, retain) LoginViewController * loginViewController;
@property (nonatomic, retain) ItemListViewController * itemlistViewController;
@property (nonatomic, retain) UITabBarController * tabBarController;
@property (nonatomic) BOOL isLoggedIn; 
@end
