//
//  OrderHistoryViewController.h
//  WowOrder
//
//  Created by Nate Flink on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistoryViewController : UIViewController <UITableViewDelegate>
@property (nonatomic, retain) TableListDataSource * dataSource;
@property (nonatomic, retain) UITableView * dataTable;
@property (nonatomic, retain) NSMutableArray * orders;
+(UITableViewCell *) addSubViewsToCell:(UITableViewCell *)cell forData:(NSDictionary *)tableCellData;
-(void)appendTableDataSourceDataForTable:(TableListDataSource *)aDataSource for:(WowOrder *)order;
-(void) updateTable:(NSString *)params;
@end
