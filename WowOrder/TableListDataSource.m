//
//  ItemListDataSource.m
//  WowOrder
//
//  Created by Nate Flink on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


@implementation TableListDataSource
@synthesize dataForTable, controller;

#pragma mark - Table view data source

- (id) init {
    if (self = [super init]) {
        dataForTable = [NSMutableArray arrayWithObjects:nil];
    }
    return self;
}

-(id) initForItemListViewController; {
    self = [self init];
    controller = @"ItemListViewController";
    return self;
}
-(id) initForOrderHistoryViewController; {
    self = [self init];
    controller = @"OrderHistoryViewController";
    return self;
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [dataForTable count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"Cell";

    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    NSDictionary * tableCellData = [dataForTable objectAtIndex:indexPath.row];
   
    if ([controller isEqualToString:@"ItemListViewController"]) {
        cell = [ItemListViewController addSubViewsToCell:cell forData:tableCellData];
    } else if ([controller isEqualToString:@"OrderHistoryViewController"]) {
        cell = [OrderHistoryViewController addSubViewsToCell:cell forData:tableCellData];
    }
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [dataForTable removeObjectAtIndex:[indexPath indexAtPosition:1]];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 * // Override to support rearranging the table view.
 * - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 * {
 * }
 */

/*
 * // Override to support conditional rearranging of the table view.
 * - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 * {
 * // Return NO if you do not want the item to be re-orderable.
 * return YES;
 * }
 */

@end
