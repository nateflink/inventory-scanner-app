//
//  RemoteObject.h
//  WowOrder
//
//  Created by Nate Flink on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RemoteObject : NSObject <NSURLConnectionDelegate>

+ (RemoteObject *)get:(NSString *)url callback:(void(^) (NSData * data))block;
+ (RemoteObject *)get:(NSString *)url success:(void(^) (NSData * data))blockSuccess error:(void(^) (NSError * data))blockError;

+ (RemoteObject *)post:(NSMutableURLRequest *)request requestBody:(NSData *)data
   callback:(void(^) (NSData * data))block;

+ (RemoteObject *)postJson:(NSString *)url requestBody:(NSData *)data callback:(void(^) (NSData * data))block;

@property (nonatomic, retain) NSMutableData * receivedData;
@property (nonatomic, retain)void(^ blockConnectionDidFinishLoading) (NSData *);
@property (nonatomic, retain)void(^ blockConnectionDidFailWithError) (NSError *);
@end
