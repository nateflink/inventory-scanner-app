//
//  SettingsViewController.m
//  WowOrder
//
//  Created by Nate Flink on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppConfiguration.h"

@implementation SettingsViewController

- (void) viewDidLoad {
    [self.view setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
    UIImageView * bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appolis_background3.png"]];
    [self.view addSubview:bg];

    [super viewDidLoad];
    [self.view addSubview:[UIView positionViewToGrid:[UIView createBigLabel:
                                                      [NSString stringWithFormat:@"SETTINGS:\n\n%@",REMOTE_SERVICE_URL]] atRow:30 andCol:22]  ];

}

@end
